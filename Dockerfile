
FROM jupyter/base-notebook

LABEL maintainer="Franck Thollard <franck.thollard@univ-grenoble-alpes.fr>"

# Fix: https://github.com/hadolint/hadolint/wiki/DL4006
# Fix: https://github.com/koalaman/shellcheck/wiki/SC3014

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

# Install all OS dependencies for fully functional notebook server
RUN apt-get update --yes && \
   # Common useful utilities
    apt-get install --yes \
    git \
    tzdata \
    unzip \
    # Inkscape is installed to be able to convert SVG files
    inkscape \
    # git-over-ssh
    openssh-client \
    # less is needed to run help in R
    # see: https://github.com/jupyter/docker-stacks/issues/1588
    less \
    # nbconvert dependencies
    # https://nbconvert.readthedocs.io/en/latest/install.html#installing-tex
    texlive-xetex \
    texlive-fonts-recommended \
    texlive-plain-generic

RUN conda install -c conda-forge proj && conda install cartopy geoviews scipy rioxarray matplotlib

# Install the package itself
RUN git clone https://gricad-gitlab.univ-grenoble-alpes.fr/deformvis/sentinel1-burt-select.git /tmp/bs ;  

WORKDIR /tmp/bs 

# to prevent caching for the git pull
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache

RUN git pull

RUN id; proj ; ls /tmp/bs ; \
    # do not install requirements here since it is done above and we do not want to have two
    # installs of jupyterlab's stuff
    # pip3 install -r requirements_no_jupyter.txt  && \
    pip3 install . 

# a ajouter en fin de traitement une fois débuggé
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* \
    rm -rf /tmp/bs && \
    apt autoremove --yes

# Create alternative for nano -> nano-tiny
# RUN update-alternatives --install /usr/bin/nano nano /bin/nano-tiny 10

# Switch back to jovyan to avoid accidental container runs as root
USER ${NB_UID}
