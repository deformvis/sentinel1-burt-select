matplotlib==3.5.1
numpy==1.22.3
geoviews==1.9.5
scipy==1.8.0
rioxarray==0.10.3
