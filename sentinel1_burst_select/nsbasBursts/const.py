#!/usr/bin/env python3
############################################################################
#
# NSBAS - New Small Baseline Chain
#
############################################################################
# Author        : Matthieu Volat (ISTerre)
############################################################################
"""Constant values

"""

C = 299792458

ae = 6378137
flat = 1.0 / 298.257223563
eccentricity2 = flat * (2-flat)
