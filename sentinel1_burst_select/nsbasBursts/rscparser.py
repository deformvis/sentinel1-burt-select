#!/usr/bin/env python3
################################################################################
#
# NSBAS - New Small Baseline Chain
#
################################################################################
# Author        : Matthieu Volat (ISTerre)
################################################################################
"""RSC file parser.

This module is a very simple RSC file parser in python to read, manipulate
write rsc files.

Example:
>>> r = rscparser.RSCParser()
>>> r.read("20030122/20030122.slc.rsc")
>>> print r.get("PROCESSING_FACILITY")
PDHS-K
>>> print r.getint("WIDTH")
5681
>>> print r.getfloat("PRF")
1652.41576

class:

RSCParser -- responsible for parsing a RSC file and managing
             the parsed database.
"""

import sys
import itertools
import logging
logger = logging.getLogger(__name__)

# from printfunctions import *

if sys.version_info[0] == 2:
    from io import open
if sys.version_info[0] >= 3:
    from collections.abc import MutableMapping
    from configparser import ParsingError, Error
    from collections import OrderedDict
elif sys.version_info[0] >= 2:
    from collections import MutableMapping
    from configparser import ParsingError, Error
    if sys.version_info[1] >= 7:
        from collections import OrderedDict
    elif sys.version_info[1] >= 4:
        from ordereddict import OrderedDict


class NoOptionError(Error):
    def __init__(self, option):
        Error.__init__(self, "No option %r" % (option))
        self.option = option
        self.args = (option)


class RSCParser(MutableMapping):
    BOOLEAN_STATES = {'1': True,  'yes': True, 'true': True,   'on': True,
                      '0': False, 'no': False, 'false': False, 'off': False}

    def __init__(self):
        self._options = OrderedDict()

    def __delitem__(self, key):
        if key not in self._options:
            raise KeyError(key)
        if key in self._options:
            del self._options[key]

    def __getitem__(self, key):
        if key in self._options:
            return self._options[key]
        else:
            raise KeyError(key)

    def __iter__(self):
        return itertools.chain(list(self._options.keys()))

    def __len__(self):
        return self._options.len()

    def __setitem__(self, key, value):
        self._options[key] = value

    def _handle_error(self, exc, fpname, lineno, line):
        if not exc:
            exc = ParsingError(fpname)
            exc.append(lineno, repr(line))
        return exc

    def _read(self, fp, fpname):
        e = None
        for lineno, line in enumerate(fp, start=1):
            # skip full comment
            if line.strip().startswith("#"):
                continue
            # strip inline comment
            index = line.find("#", 1)
            if index == 0 or (index > 0 and line[index-1].isspace()):
                line = line[:index]
            # strip remaining white spaces
            line = line.strip()
            # skip empty
            if (not line):
                continue
            # split line in option/value couple
            name, val = None, None
            try:
                name, val = line.split(None, 1)
            except ValueError:
                # Technicaly, this is should be fatal, but try to work around
                # it, some of our scripts are made to cope with the error:
                # the others will have their failure when accessing the None
                logger.warning("Malformed file "+fpname+" at line "+str(lineno)+": no value")
                # print_warn("Malformed file "+fpname+" at line "+str(lineno)+": no value")
                name = line
                val = None
            except Exception:
                e = self._handle_error(e, fpname, lineno, line)
            if name not in self._options:
                self._options[name] = val
            elif isinstance(self._options[name], list):
                self._options[name].append(val)
            else:
                self._options[name] = [self._options[name], val]
        if e:
            raise e

    def has_option(self, option):
        return option in self._options

    def options(self):
        return list(self._options.keys())

    def read(self, filenames, encoding=None):
        read_ok = []
        if isinstance(filenames, str) or isinstance(filenames, unicode):
            filenames = [filenames]
        for filename in filenames:
            try:
                with open(filename, encoding=encoding) as fp:
                    self._read(fp, filename)
            except IOError:
                continue
            read_ok.append(filename)
        return read_ok

    def read_file(self, f, source=None):
        if source is None:
            try:
                source = f.name
            except AttributeError:
                source = "<???>"
        self._read(f, source)

    def get(self, option):
        if option in self:
            return self[option]
        else:
            raise NoOptionError(option)

    def getint(self, option):
        return int(self.get(option))

    def getfloat(self, option):
        return float(self.get(option))

    def getboolean(self, option):
        value = self.get(option)
        if value.lower() not in self.BOOLEAN_STATES:
            raise ValueError('Not a boolean: %s' % value)
        return self.BOOLEAN_STATES[value.lower()]

    def items(self):
        d = self._options.copy()
        if "__name__" in d:
            del d["__name__"]
        return list(d.items())

    def remove_option(self, option):
        existed = option in self._options
        if existed:
            del self._options[option]
        return existed

    def set(self, option, value):
        self._options[option] = value

    def write(self, f):
        for key, value in list(self._options.items()):
            if isinstance(value, list):
                for subvalue in value:
                    f.write("%-40s %-30s\n" % (key, subvalue))
            else:
                f.write("%-40s %-30s\n" % (key, value))
