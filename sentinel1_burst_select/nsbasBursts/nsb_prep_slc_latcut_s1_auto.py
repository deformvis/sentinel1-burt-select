#!/usr/bin/env python3
# -*- coding: utf-8 -*-
############################################################################
#
# NSBAS - New Small Baseline Chain
#
############################################################################
# Authors       : Matthieu Volat (ISTerre)
#                 Raphael Grandin (IPGP)
############################################################################
"""\
nsb_prep_slc_latcut_s1_auto.py
---------------
Select Sentinel-1 bursts for a collection of SAFE products
Includes a prser for Sentinel-1 metadata (safe2rsc).
Capable of processing several (possibly zipped) SAFE directories at the same time.
When given a 3D coordinates of a target, calculates how to crop the TIFFs around the target.


"""



import collections
import datetime, getopt, glob, os, sys, math, re
import xml.etree.ElementTree
import numpy as np
import zipfile
from io import StringIO

from scipy.interpolate import NearestNDInterpolator, LinearNDInterpolator

import nsbasBursts
import nsbasBursts.ziputils as nsb_ziputils
import nsbasBursts.nsbio as nsb_io
import nsbasBursts.procparser as nsb_procparser
import nsbasBursts.utils as nsb_utils

import pandas as pd

class nsb_S1_burst_collection:

    # function to convert from ISO to seconds since midnight
    def convIsoToSecond(self, iterable):
        result = []
        for element in iterable:
            # works with Python version >= 2.7
            # result.append((element-element.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds())
            # for Python version < 2.7
            t = element.time()
            result.append(t.hour*3600 + t.minute*60 + t.second + t.microsecond/1000000.0)
        return result
    
    # Combine info from multiple XML files into a single element tree
    def myXMLmerge(self, tree1, tree2):
        return file1
    
    def checkFieldIsEqual(self, tree1, tree2, fieldName):
        fieldValue1 = tree1.find(fieldName).text
        fieldValue2 = tree2.find(fieldName).text
        if fieldValue1 != fieldValue2:
            sys.stderr.write("\rWarning : field %s different!\n" % fieldName)

    # Read metadata from Sentinel-1 SAFE product
    def safe2rsc(self, sardir, mode = 'iw1', polarization = "vv", return_geoloc_corners=False, verbose=0, write_output=False, target_lon = None, target_lat = None, target_alt = float(200.0), crop_lat_km=None, crop_lon_km=None, outdir='.'):

        logger = nsb_utils.change_log_level(verbose ,[nsb_io, nsb_procparser, nsb_utils])

        # Search for candidate inputs, sorting them in chronological order
        # TODO : sorting may fail in cases where both SSV (single po)
        # and SDV (dual pol) acquisitions coexist
        logger.info(" Processing date: %s" % sardir)
        args = self.SAFEsInSarDirs[sardir]
        logger.debug("  Processing safe files: %s" % args)

        # set a few numbers
        C = 299792458.0 # Speed of light
        EARTH_GM=3.98618328e+14 # Earth's mass multiplied by G
        EARTH_SPINRATE=7.29211573052e-05 # Earth's spin rate
        
        # Remote directory where SLCs are stored on the cloud
        dir_remote='http://sentinel1-slc-seasia-pds.s3-website-ap-southeast-1.amazonaws.com/datasets/slc/v1.1'
        
        # set a few variables
        lenExtrapOrbit=200 # duration of extrapolation of orbit state vectors before and after given segment
        pixel_ratio=0.25   # Sentinel-1 IW pixel ratio (range pixel size / azimuth pixel size)
        
        # list of parameters that should share the same value among the different files
        listOfValsToCheck=["IPFVERSION","WIDTH","XMAX", \
                "ORBIT_NUMBER","PLATFORM","POLARIZATION","ORBIT_DIRECTION", \
                "WAVELENGTH","PRF","PULSE_LENGTH","CHIRP_SLOPE","STARTING_RANGE"]
        listOfBurstValsToCheck=["linesPerBurst","samplesPerBurst","azimuthTimeInterval","azimuthSteeringRate", \
                "rangeSamplingRate","slantRangeTime","radarFrequency"]
    
        # # # # # # # # # # # # # # # # # #
        # 0. Read arguments passed to python script
        #try:
        #    opts, args = getopt.getopt(sys.argv[1:], "m:p:o:n:hvfw", ['lat=', 'lon=', 'alt=', 'crop_lat_km=', 'crop_lon_km='])
        #except getopt.GetoptError as err:
        #    # print help information and exit:
        #    print(str(err))  # will print something like "option -a not recognized"
        #    usage()
        #    sys.exit(2)
        #mode = "1"
        #polarization = "vv"
        #outdir = ""
        #writeRsc = False
        #verbose = False
        #target_lon = None
        #target_lat = None
        #target_alt = float(200.0)
        #for o, a in opts:
        #    # sub-swath
        #    if o == "-m":
        #        if len(a)==1: # assumes this sub-swath number in IW mode (can be 1, 2 or 3)
        #            mode = "iw" + a.lower()
        #        else: # can be iw1, iw2, iw3 (IW mode) or s1, s2, s3, s4, s5, s6 (SM mode)
        #            mode = a
        #    # polarization (can be vv, vh, hh or hv)
        #    elif o == "-p":
        #        polarization = a.lower()
        #    elif o == "-o":
        #        outdir = a
        #    elif o in ("--lon"):
        #        target_lon = float(a)
        #    elif o in ("--lat"):
        #        target_lat = float(a)
        #    elif o in ("--alt"):
        #        target_alt = float(a)
        #    elif o in ("--crop_lat_km"):
        #        crop_lat_km = float(a)
        #    elif o in ("--crop_lon_km"):
        #        crop_lon_km = float(a)
        #    elif o == "-n":
        #        prefix = a
        #    elif o in ("-h", "--help"):
        #        usage()
        #        sys.exit()
        #    elif o == "-w":
        #        writeRsc = True
        #    elif o == "-z":
        #        write_output = True
        #    elif o == "-v":
        #        verbose = True
        #        logger = nsb_utils.change_log_level(4 ,[nsb_io, nsb_procparser, nsb_utils])
    
        #if verbose:
        #    logger = nsb_utils.change_log_level(4 ,[nsb_io, nsb_procparser, nsb_utils])
        
        valsMerge = collections.OrderedDict()
        burstValsMerge = collections.OrderedDict()
        
        # # # # # # # # # # # # # # # # # #
        # 1. Find the XML annotation file
        # number of files provided by user
        numOfFiles = len(args)
        # initialize lists
        safepath = []
        safedir = []
        manifestfile = []
        annoxmlfiles = []
        safeslist = []
        measurementPath = []
        measurementTime = []
        read_from_zip = []
        logger.info("Reading %d products for %s..." %(numOfFiles, sardir))
        
        for fileNumber in range(numOfFiles):
            safepath.append(args[fileNumber])
            zippedSafe, zipIsLocal, zipPrefixVSI = nsb_ziputils.check_SAFE_type_zip_local(args[fileNumber], logger)
            if zippedSafe:
                read_from_zip.append(True)
                logger.info(" %d / %d : zipped SAFE : %s" % (fileNumber+1, numOfFiles, os.path.basename(args[fileNumber])))
                test_filein_zip = args[fileNumber]
                filelisting_in_zip = nsb_ziputils.get_zip_file_listing(test_filein_zip, zipIsLocal, logger)
                filein_zip_safename = nsb_ziputils.get_SAFE_name_from_Zip(filelisting_in_zip, logger)
                measurementPathTmp = nsb_ziputils.get_TIFF_name_from_Zip(filelisting_in_zip, mode, polarization, logger)
                annoxmlfilesTmp = os.path.splitext(measurementPathTmp)[0] + '.xml'
                logger.debug("zipIsLocal     : %s" % zipIsLocal)
                logger.debug("zipFullVSIPath : %s%s" % (zipPrefixVSI, test_filein_zip))
                logger.debug("safename       : %s" % filein_zip_safename)
                logger.debug("filein_zip_tif : %s" % measurementPathTmp)
                logger.debug("filein_zip_xml : %s" % annoxmlfilesTmp)
                safedir.append(filein_zip_safename) 
                manifestfile.append(filein_zip_safename+'/manifest.safe')
                safeslist.append(test_filein_zip)
                annoxmlfiles.append(os.path.join(filein_zip_safename, 'annotation/', annoxmlfilesTmp))
                logger.info(" and SAFE directory : %s" % filein_zip_safename)
            elif os.path.splitext(args[fileNumber])[-1] == '.SAFE':
                read_from_zip.append(False)
                safeslist.append(None)
                safedir.append(os.path.basename(args[fileNumber]))
                logger.info(" %d / %d : SAFE : %s" % (fileNumber+1, numOfFiles, os.path.basename(args[fileNumber])))
                globpattern = os.path.join(safepath[fileNumber],"manifest.safe")
                # Manifest file
                manifestfile.append(glob.glob(os.path.join(globpattern))[0])
                # Create list of xml files corresponding to parameters given
                globpattern = os.path.join(safepath[fileNumber],
                                   "annotation",
                                    "*-%s-slc-%s-*.xml" % (mode or "*",
                                                           polarization or "*"))
                annoxmlfiles.append(glob.glob(os.path.join(globpattern))[0])
            else:
                logger.error("Error : file type not recognized for %s" % args[fileNumber])
                sys.exit(1)
            basenameFile = os.path.basename(annoxmlfiles[fileNumber])
            measurementTimeTempo = basenameFile.split("-")[4]
            measurementTimeHour = int(measurementTimeTempo[9:11])
            measurementTimeMin = int(measurementTimeTempo[11:13])
            measurementTimeSec = int(measurementTimeTempo[13:15])
            measurementTime.append( measurementTimeHour*3600 + measurementTimeMin*60 + measurementTimeSec )
            measurementPath.append(os.path.join(safepath[fileNumber],"measurement","%s.tiff" % os.path.splitext(basenameFile)[0] ))
            logger.info(" %d / %d : image : %s" % (fileNumber+1, numOfFiles, os.path.basename(measurementPath[fileNumber])))
    
        
        
        # Reorder files according to increasing acquisition time
        listOrderFiles = np.argsort(measurementTime)
        
        # Initialize orbital info
        orbTime = []
        orbPosX = []
        orbPosY = []
        orbPosZ = []
        orbVelX = []
        orbVelY = []
        orbVelZ = []
        orbTimeSeconds = []
        
        # Initialize Doppler info
        dcAzimuthTime = []
        dcT0 = []
        dataDcPolynomialOrder0 = []
        dataDcPolynomialOrder1 = []
        dataDcPolynomialOrder2 = []
        dataDcPolynomialOrder3 = []
        dataDcPolynomialOrder4 = []
        geometryDcPolynomialOrder0 = []
        geometryDcPolynomialOrder1 = []
        geometryDcPolynomialOrder2 = []
        geometryDcPolynomialOrder3 = []
        geometryDcPolynomialOrder4 = []
        dcAzimuthTimeSeconds = []
        
        # Initialize azimuth FM rate info
        azFMRateTime = []
        azFMRatet0 = []
        azFMRatec0 = []
        azFMRatec1 = []
        azFMRatec2 = []
        azFMRateTimeSeconds = []
        
        # Initialize burst info
        burstAzimuthTime = []
        burstAzimuthAnxTime = []
        burstByteOffset = []
        burstFirstValidSample = []
        burstLastValidSample = []
        burstAzimuthTimeSeconds = []
        burstLatitude = []
        
        # Initialize antenna pattern info
        antennaPatternAzimuthTime = []
        antennaPatternSlantRangeTime = []
        antennaPatternIncidenceAngle = []
        antennaPatternTimeSeconds = []
        
        # Initialize geolocation tie points
        geolocationTime = []
        geolocationLatitude = []
        geolocationLongitude = []
        geolocationSlantRangeTime = []
        geolocationIncidence = []
        
        #Orbit duration in s
        orbit_duration = 1.64575430405602567843*3600
        
        # Save SAFE directory name associated with each burst
        burstSafedir = []
        
        # Loop over files
        for fileNumber in listOrderFiles:
        
            vals = collections.OrderedDict()
            burstVals = collections.OrderedDict()
        
            # # # # # # # # # # # # # # # # # #
            # 0. First check that we need to work on a zipped SAFE, or a SAFE
            zippedSafe, zipIsLocal, zipPrefixVSI = nsb_ziputils.check_SAFE_type_zip_local(safepath[fileNumber], logger)
        
            # # # # # # # # # # # # # # # # # #
            # 1. Read IPF version in manifest (grep)
            vals["IPFVERSION"] = float(nsb_ziputils.get_ipfversion_from_manifest(safepath[fileNumber], zippedSafe, None, logger))
            logger.debug("IPF version = {0:.2f}".format(vals["IPFVERSION"]))
        
            # # # # # # # # # # # # # # # # # #
            # 2. Open & parse
            t = nsb_ziputils.open_annotationxml(safepath[fileNumber], mode, polarization, zippedSafe, zipIsLocal, logger)
        
            # # # # # # # # # # # # # # # # # #
            # 3. Extract the wanted values from xml
            vals["WIDTH"] = t.find("swathTiming/samplesPerBurst").text
            if vals["WIDTH"] == "0":
                vals["WIDTH"] = t.find("imageAnnotation/imageInformation/numberOfSamples").text
            vals["XMIN"] = "0"
            vals["XMAX"] = int(vals["WIDTH"])-1
            vals["FILE_LENGTH"] = t.find("imageAnnotation/imageInformation/numberOfLines").text
            vals["YMIN"] = "0"
            vals["YMAX"] = int(vals["FILE_LENGTH"])-1
            vals["ORBIT_NUMBER"] = t.find("adsHeader/absoluteOrbitNumber").text
            vals["PLATFORM"] = t.find("adsHeader/missionId").text
            vals["POLARIZATION"] = "/".join(list(t.find("adsHeader/polarisation").text))
            vals["ORBIT_DIRECTION"] = "ascending" if t.find("generalAnnotation/productInformation/pass").text == "Ascending" else "descending"
            vals["HEADING"] = float(t.find("generalAnnotation/productInformation/platformHeading").text)
            #vals["radarFrequency"] = t.find("generalAnnotation/productInformation/radarFrequency").text
            vals["WAVELENGTH"] = C / float(t.find("generalAnnotation/productInformation/radarFrequency").text)
        
        
            # # The PRF provided in the XML does not yield the actual azimuth pixel size according to ROI_PAC's formula
            # vals["PRF"] = 1.0 / float(t.find("generalAnnotation/downlinkInformationList/downlinkInformation/downlinkValues/pri").text)
        
            # PRF fix : we cheat by altering the PRF value so that ROI_PAC finds the right azimuth pixel size
            vals["PRF"] = 1.0 / float(t.find("imageAnnotation/imageInformation/azimuthTimeInterval").text)
        
            vals["PULSE_LENGTH"] = float(t.find("generalAnnotation/downlinkInformationList/downlinkInformation/downlinkValues/txPulseLength").text)
            vals["CHIRP_SLOPE"] = float(t.find("generalAnnotation/downlinkInformationList/downlinkInformation/downlinkValues/txPulseRampRate").text)
            vals["STARTING_RANGE"] = C / 2 * float(t.find("imageAnnotation/imageInformation/slantRangeTime").text)
        
            firstLineTime=(datetime.datetime.strptime(t.find("imageAnnotation/imageInformation/productFirstLineUtcTime").text, "%Y-%m-%dT%H:%M:%S.%f" ))
            vals["FIRST_LINE_YEAR"] = firstLineTime.year
            vals["FIRST_LINE_MONTH_OF_YEAR"] = firstLineTime.month
            vals["FIRST_LINE_DAY_OF_MONTH"] = firstLineTime.day
            vals["FIRST_LINE_HOUR_OF_DAY"] = firstLineTime.hour
            vals["FIRST_LINE_MN_OF_HOUR"] = firstLineTime.minute
            vals["FIRST_LINE_S_OF_MN"] = firstLineTime.second
            vals["FIRST_LINE_MS_OF_S"] = firstLineTime.microsecond / 1000
        
            lastLineTime=(datetime.datetime.strptime(t.find("imageAnnotation/imageInformation/productLastLineUtcTime").text, "%Y-%m-%dT%H:%M:%S.%f" ))
        
            vals["FIRST_LINE_STRING"] = firstLineTime
            vals["LAST_LINE_STRING"] = lastLineTime
            vals["FIRST_LINE_UTC"] = self.convIsoToSecond([firstLineTime])[0]
            vals["LAST_LINE_UTC"] = self.convIsoToSecond([lastLineTime])[0]
        
            vals["RANGE_PIXEL_SIZE"] = float(t.find("imageAnnotation/imageInformation/rangePixelSpacing").text)
            vals["AZIMUTH_PIXEL_SIZE"] = float(t.find("imageAnnotation/imageInformation/azimuthPixelSpacing").text)
        
            vals["RANGE_SAMPLING_FREQUENCY"] = float(t.find("generalAnnotation/productInformation/rangeSamplingRate").text)
        
            vals["EQUATORIAL_RADIUS"] = float(t.find("imageAnnotation/processingInformation/ellipsoidSemiMajorAxis").text)
            vals["EARTH_RADIUS"] = float(t.find("imageAnnotation/processingInformation/ellipsoidSemiMinorAxis").text)
            vals["ECCENTRICITY_SQUARED"] = 1.0 - float(vals["EARTH_RADIUS"])/float(vals["EQUATORIAL_RADIUS"])
        
            vals["PLANET_GM"] = EARTH_GM
            vals["PLANET_SPINRATE"] = EARTH_SPINRATE
        
            # The SLCs are processed to Zero Doppler
            vals["DOPPLER_RANGE0"]=0
            vals["DOPPLER_RANGE1"]=0
            vals["DOPPLER_RANGE2"]=0
            vals["DOPPLER_RANGE3"]=0
        
        
            # # # # # # # # # # # # # # # # # #
            # 4. Read orbit information
            orbit_list = t.find("generalAnnotation/orbitList")
            for orbit in t.findall("generalAnnotation/orbitList/orbit"):
                orbTime.append(datetime.datetime.strptime(orbit.find('time').text, "%Y-%m-%dT%H:%M:%S.%f" ))
                orbPosX.append(float(orbit.find('position/x').text))
                orbPosY.append(float(orbit.find('position/y').text))
                orbPosZ.append(float(orbit.find('position/z').text))
                orbVelX.append(float(orbit.find('velocity/x').text))
                orbVelY.append(float(orbit.find('velocity/y').text))
                orbVelZ.append(float(orbit.find('velocity/z').text))
        
        
            # # # # # # # # # # # # # # # # # #
            # 5. Read doppler centroid polynomials
            dcEstimate_list = t.find("dopplerCentroid/dcEstimateList")
            for dcEstimate in t.findall("dopplerCentroid/dcEstimateList/dcEstimate"):
                dcAzimuthTime.append(datetime.datetime.strptime(dcEstimate.find('azimuthTime').text, "%Y-%m-%dT%H:%M:%S.%f" ))
                dcT0.append(float(dcEstimate.find('t0').text))
                # data
                myDataDcPolynomial=dcEstimate.find('dataDcPolynomial').text
                myDataDcPolynomial = myDataDcPolynomial.split()
                dataDcPolynomialOrder0.append(float(myDataDcPolynomial[0]))
                dataDcPolynomialOrder1.append(float(myDataDcPolynomial[1]))
                dataDcPolynomialOrder2.append(float(myDataDcPolynomial[2]))
                dataDcPolynomialOrder3.append(float(0))
                dataDcPolynomialOrder4.append(float(0))
                # geometry
                myGeometryDcPolynomial=dcEstimate.find('geometryDcPolynomial').text
                myGeometryDcPolynomial = myGeometryDcPolynomial.split()
                geometryDcPolynomialOrder0.append(float(myGeometryDcPolynomial[0]))
                geometryDcPolynomialOrder1.append(float(myGeometryDcPolynomial[1]))
                geometryDcPolynomialOrder2.append(float(myGeometryDcPolynomial[2]))
                geometryDcPolynomialOrder3.append(float(0))
                geometryDcPolynomialOrder4.append(float(0))
            # print(dcAzimuthTime[0],dcAzimuthTimeSeconds[0])
            # print(dataDcPolynomialOrder0)
        
        
            # # # # # # # # # # # # # # # # # #
            # 6. Read azimuth FM rate polynomial
            for azFMRate in t.findall("generalAnnotation/azimuthFmRateList/azimuthFmRate"):
                azFMRateTime.append(datetime.datetime.strptime(azFMRate.find('azimuthTime').text, "%Y-%m-%dT%H:%M:%S.%f" ))
                azFMRatet0.append(float(azFMRate.find('t0').text))
                # Decoding depends on IPF version
                if(float(vals["IPFVERSION"]) >= 2.43):
                    ### <safe:software name="Sentinel-1 IPF" version="002.43"/>
                    azFMRatePoly= (azFMRate.find('azimuthFmRatePolynomial').text).split()
                    azFMRatec0.append(float(azFMRatePoly[0]))
                    azFMRatec1.append(float(azFMRatePoly[1]))
                    azFMRatec2.append(float(azFMRatePoly[2]))
                else:
                    ### <safe:software name="Sentinel-1 IPF" version="002.36"/>
                    azFMRatec0.append(float(azFMRate.find('c0').text))
                    azFMRatec1.append(float(azFMRate.find('c1').text))
                    azFMRatec2.append(float(azFMRate.find('c2').text))
            # print(azFMRateTime[0],azFMRateTimeSeconds[0])
            # print(azFMRateTime)
            # print(azFMRatet0)
            # print(azFMRatec0)
        
        
            # # # # # # # # # # # # # # # # # #
            # 7. Read burst time
            for burst in t.findall("swathTiming/burstList/burst"):
                burstAzimuthTime.append(datetime.datetime.strptime(burst.find('azimuthTime').text, "%Y-%m-%dT%H:%M:%S.%f" ))
                anxt = float(burst.find('azimuthAnxTime').text)
                if (anxt > orbit_duration) :
                    anxt=anxt-orbit_duration
                burstAzimuthAnxTime.append(anxt)
                burstByteOffset.append(burst.find('byteOffset').text)
                burstFirstValidSample.append((burst.find('firstValidSample').text).split())
                burstLastValidSample.append((burst.find('lastValidSample').text).split())
                #burstSafedir.append( safedir[fileNumber] )
                burstSafedir.append( safepath[fileNumber] )
        
            # print(burstFirstValidSample)
            # print(len(burstFirstValidSample[1]))
            # print(burstAzimuthTime)
            # print(burstByteOffset)
        
        
            # # # # # # # # # # # # # # # # # #
            # 8. Read incidence angles
            for antennaPattern in t.findall("antennaPattern/antennaPatternList/antennaPattern"):
                antennaPatternAzimuthTime.append(datetime.datetime.strptime(antennaPattern.find('azimuthTime').text, "%Y-%m-%dT%H:%M:%S.%f" ))
                antennaPatternSlantRangeTime.append((antennaPattern.find('slantRangeTime').text).split())
                antennaPatternIncidenceAngle.append((antennaPattern.find('incidenceAngle').text).split())
            #print(antennaPatternSlantRangeTime)
            #print(len(antennaPatternSlantRangeTime[1]))
            #print(len(antennaPatternSlantRangeTime))
            # print(burstAzimuthTime)
            # print(burstByteOffset)
        
        
            # # # # # # # # # # # # # # # # # #
            # 9. Read remaining information
        
            burstVals["linesPerBurst"] = int(t.find("swathTiming/linesPerBurst").text)
            burstVals["samplesPerBurst"] = int(t.find("swathTiming/samplesPerBurst").text)
            burstVals["azimuthTimeInterval"] = float(t.find("imageAnnotation/imageInformation/azimuthTimeInterval").text)
            burstVals["azimuthSteeringRate"] = float(t.find("generalAnnotation/productInformation/azimuthSteeringRate").text)
            burstVals["rangeSamplingRate"] = float(t.find("generalAnnotation/productInformation/rangeSamplingRate").text)
            burstVals["slantRangeTime"] = float(t.find("imageAnnotation/imageInformation/slantRangeTime").text)
            burstVals["radarFrequency"] = float(t.find("generalAnnotation/productInformation/radarFrequency").text)
            #burstVals["zeroDopMinusAcqTime"] = float(t.find("imageAnnotation/imageInformation/zeroDopMinusAcqTime").text)
        
            # # # # # # # # # # # # # # # # # #
            # 10. Read geolocation information
            for geolocationGridPoint in t.findall("geolocationGrid/geolocationGridPointList/geolocationGridPoint"):
                geolocationTime.append(datetime.datetime.strptime(geolocationGridPoint.find('azimuthTime').text, "%Y-%m-%dT%H:%M:%S.%f" ))
        #            geolocationSlantRangeTime.append((geolocationGridPoint.find('slantRangeTime').text).split())
                geolocationSlantRangeTime.append(float(geolocationGridPoint.find('slantRangeTime').text))
        #            geolocationLatitude.append(float(geolocationGridPoint.find('latitude')))
                geolocationLatitude.append(float(geolocationGridPoint.find('latitude').text))
                geolocationLongitude.append(float(geolocationGridPoint.find('longitude').text))
                geolocationIncidence.append(float(geolocationGridPoint.find('incidenceAngle').text))
        
            # # # # # # # # # # # # # # # # # #
            # Initialize / update / check metadata
        
            if fileNumber == listOrderFiles[0]:
                valsMerge = vals
                burstValsMerge = burstVals
        
            else:
                # Update last line time
                valsMerge["LAST_LINE_UTC"] = vals["LAST_LINE_UTC"]
                valsMerge["LAST_LINE_STRING"] = vals["LAST_LINE_STRING"]
        
                # Check consistency
                for parameter in listOfValsToCheck:
                    if valsMerge[parameter] != vals[parameter]:
                        sys.stderr.write("\rWarning : parameter %s not identical in the files!\n" % (parameter))
                        sys.stderr.write("\r Conflicting values : %s / %s\n" % (valsMerge[parameter], vals[parameter]))
        
                for parameter in listOfBurstValsToCheck:
                    if burstValsMerge[parameter] != burstVals[parameter]:
                        sys.stderr.write("\rWarning : parameter %s not identical in the files!\n" % (parameter))
                        sys.stderr.write("\r Conflicting values : %s / %s\n" % (burstValsMerge[parameter], burstVals[parameter]))
        
        # # # # # # # # # # # # # # # # # #
        # Convert times
        
        orbTimeSeconds=self.convIsoToSecond(orbTime)
        dcAzimuthTimeSeconds=self.convIsoToSecond(dcAzimuthTime)
        azFMRateTimeSeconds=self.convIsoToSecond(azFMRateTime)
        burstAzimuthTimeSeconds=self.convIsoToSecond(burstAzimuthTime)
        antennaPatternTimeSeconds=self.convIsoToSecond(antennaPatternAzimuthTime)
        geolocationTimeSeconds=self.convIsoToSecond(geolocationTime)
        
        # # # # # # # # # # # # # # # # # #
        # slant range time to range
        geolocationRange = np.asarray(geolocationSlantRangeTime)*C/2.0
        
        # # # # # # # # # # # # # # # # # #
        # interpolate to extract latitude at burst start time
        burstLatitude = np.interp( np.asarray(burstAzimuthTimeSeconds), np.asarray(geolocationTimeSeconds), np.asarray(geolocationLatitude) )
        burstLongitude = np.interp( np.asarray(burstAzimuthTimeSeconds), np.asarray(geolocationTimeSeconds), np.asarray(geolocationLongitude) )
        
        # # # # # # # # # # # # # # # # # #
        # Compute centre time
        
        firstLineTime = valsMerge["FIRST_LINE_STRING"]
        lastLineTime = valsMerge["LAST_LINE_STRING"]
        centerLineTime=firstLineTime+(lastLineTime - firstLineTime)/2
        valsMerge["CENTER_LINE_UTC"] = self.convIsoToSecond([centerLineTime])[0]
        valsMerge["FIRST_FRAME_SCENE_CENTER_TIME"] = centerLineTime.strftime("%Y%m%d%H%M%S")
        valsMerge["DATE"] = centerLineTime.strftime("%Y%m%d")
        
        
        # # # # # # # # # # # # # # # # # # # # # # #
        # Interpolate / extrapolate state vectors
        # # # # # # # # # # # # # # # # # # # # # # #
        
        # calculate scalar velocity in the middle of the orbit
        
        # Only keep unique records
        orbit_array, index_uniques = np.unique(np.array( [orbTimeSeconds, orbPosX, orbPosY, orbPosZ, orbVelX, orbVelY, orbVelZ] ), axis=1, return_index=True)
        orbTimeSeconds= [orbTimeSeconds[j] for j in index_uniques]
        orbTime = [orbTime[j] for j in index_uniques]
        orbPosX = [orbPosX[j] for j in index_uniques]
        orbPosY = [orbPosY[j] for j in index_uniques]
        orbPosZ = [orbPosZ[j] for j in index_uniques]
        orbVelX = [orbVelX[j] for j in index_uniques]
        orbVelY = [orbVelY[j] for j in index_uniques]
        orbVelZ = [orbVelZ[j] for j in index_uniques]
        
        #we need to sort orbit state vectors first
        orbPosX = [x for (y,x) in sorted(zip(orbTime,orbPosX))]
        orbPosY = [x for (y,x) in sorted(zip(orbTime,orbPosY))]
        orbPosZ = [x for (y,x) in sorted(zip(orbTime,orbPosZ))]
        orbVelX = [x for (y,x) in sorted(zip(orbTime,orbVelX))]
        orbVelY = [x for (y,x) in sorted(zip(orbTime,orbVelY))]
        orbVelZ = [x for (y,x) in sorted(zip(orbTime,orbVelZ))]
        orbTimeSeconds = [x for (y,x) in sorted(zip(orbTime,orbTimeSeconds))]
        orbTime = sorted(orbTime)
        
        #print("Raw state vectors: ")
        #for myt, mypx, mypy, mypz, myvx, myvy, myvz in zip(orbTimeSeconds, orbPosX, orbPosY, orbPosZ, orbVelX, orbVelY, orbVelZ):
        #   print(myt, mypx, mypy, mypz, myvx, myvy, myvz)
        
        # extract velocity middle index
        indexOrbMiddle=int(round(len(orbTime)//2))
        orbVelocity=math.sqrt(math.pow(orbVelX[indexOrbMiddle],2)+math.pow(orbVelY[indexOrbMiddle],2)+math.pow(orbVelZ[indexOrbMiddle],2))
        valsMerge["VELOCITY"] = orbVelocity
        
        # # extrapolate orbits using third degree polynomial
        # positions
        orbPosXExtrapPoly = np.poly1d(np.polyfit(orbTimeSeconds, orbPosX, deg=3))
        orbPosYExtrapPoly = np.poly1d(np.polyfit(orbTimeSeconds, orbPosY, deg=3))
        orbPosZExtrapPoly = np.poly1d(np.polyfit(orbTimeSeconds, orbPosZ, deg=3))
        # velocities
        orbVelXExtrapPoly = np.poly1d(np.polyfit(orbTimeSeconds, orbVelX, deg=3))
        orbVelYExtrapPoly = np.poly1d(np.polyfit(orbTimeSeconds, orbVelY, deg=3))
        orbVelZExtrapPoly = np.poly1d(np.polyfit(orbTimeSeconds, orbVelZ, deg=3))
        
        # # sample extrapolated orbit at appropriate rate
        # # build list of dates before first line
        orbTimeSecondsBef=np.linspace(orbTimeSeconds[0]-lenExtrapOrbit,orbTimeSeconds[0],num=lenExtrapOrbit,endpoint=False)
        # # build list of dates between first line and last line
        orbTimeSecondsAll=np.linspace(orbTimeSeconds[0]-lenExtrapOrbit,orbTimeSeconds[len(orbTimeSeconds)-1]+lenExtrapOrbit,
                num=int(orbTimeSeconds[len(orbTimeSeconds)-1]-orbTimeSeconds[0]+(lenExtrapOrbit*2)),
                endpoint=False)
        # # build list of dates after last line
        orbTimeSecondsAft=np.linspace(orbTimeSeconds[len(orbTimeSeconds)-1]+1,orbTimeSeconds[len(orbTimeSeconds)-1]+lenExtrapOrbit,num=lenExtrapOrbit)
        
        # # do the sampling
        # positions
        orbPosXExtrapValBef=np.polyval(orbPosXExtrapPoly,orbTimeSecondsBef)
        orbPosYExtrapValBef=np.polyval(orbPosYExtrapPoly,orbTimeSecondsBef)
        orbPosZExtrapValBef=np.polyval(orbPosZExtrapPoly,orbTimeSecondsBef)
        
        orbPosXExtrapValAll=np.polyval(orbPosXExtrapPoly,orbTimeSecondsAll)
        orbPosYExtrapValAll=np.polyval(orbPosYExtrapPoly,orbTimeSecondsAll)
        orbPosZExtrapValAll=np.polyval(orbPosZExtrapPoly,orbTimeSecondsAll)
        
        orbPosXExtrapValAft=np.polyval(orbPosXExtrapPoly,orbTimeSecondsAft)
        orbPosYExtrapValAft=np.polyval(orbPosYExtrapPoly,orbTimeSecondsAft)
        orbPosZExtrapValAft=np.polyval(orbPosZExtrapPoly,orbTimeSecondsAft)
        # velocities
        orbVelXExtrapValBef=np.polyval(orbVelXExtrapPoly,orbTimeSecondsBef)
        orbVelYExtrapValBef=np.polyval(orbVelYExtrapPoly,orbTimeSecondsBef)
        orbVelZExtrapValBef=np.polyval(orbVelZExtrapPoly,orbTimeSecondsBef)
        
        orbVelXExtrapValAll=np.polyval(orbVelXExtrapPoly,orbTimeSecondsAll)
        orbVelYExtrapValAll=np.polyval(orbVelYExtrapPoly,orbTimeSecondsAll)
        orbVelZExtrapValAll=np.polyval(orbVelZExtrapPoly,orbTimeSecondsAll)
        
        orbVelXExtrapValAft=np.polyval(orbVelXExtrapPoly,orbTimeSecondsAft)
        orbVelYExtrapValAft=np.polyval(orbVelYExtrapPoly,orbTimeSecondsAft)
        orbVelZExtrapValAft=np.polyval(orbVelZExtrapPoly,orbTimeSecondsAft)
        
        orbVelocity=[]
        orbTimeSecondsConcat=[]
        orbPosXConcat=[]
        orbPosYConcat=[]
        orbPosZConcat=[]
        orbVelXConcat=[]
        orbVelYConcat=[]
        orbVelZConcat=[]
        
        
        # # Concatenate the orbits into a single list of state vectors
        # full arc
        lenOrbAll=len(orbTimeSecondsAll)
        for stateVecNum in range(0,lenOrbAll):
            velocCurrent=math.sqrt(math.pow(orbVelXExtrapValAll[stateVecNum],2)+math.pow(orbVelYExtrapValAll[stateVecNum],2)+math.pow(orbVelZExtrapValAll[stateVecNum],2))
            orbVelocity.append(float(velocCurrent))
            orbTimeSecondsConcat.append(float(orbTimeSecondsAll[stateVecNum]))
            orbPosXConcat.append(float(orbPosXExtrapValAll[stateVecNum]))
            orbPosYConcat.append(float(orbPosYExtrapValAll[stateVecNum]))
            orbPosZConcat.append(float(orbPosZExtrapValAll[stateVecNum]))
            orbVelXConcat.append(float(orbVelXExtrapValAll[stateVecNum]))
            orbVelYConcat.append(float(orbVelYExtrapValAll[stateVecNum]))
            orbVelZConcat.append(float(orbVelZExtrapValAll[stateVecNum]))
            # print( orbTimeSecondsAft[stateVecNum],orbPosXExtrapValAft[stateVecNum],orbPosYExtrapValAft[stateVecNum],orbPosZExtrapValAft[stateVecNum],orbVelXExtrapValAft[stateVecNum],orbVelYExtrapValAft[stateVecNum],orbVelZExtrapValAft[stateVecNum],velocCurrent)
        
        # print(orbTime[0],orbTimeSeconds[0])
        # print(orbTime)
        # print(orbPosX)
        # print(orbVelY)
        
        
        
        # # # # # # # # # #
        # Incidence angle
        # # # # # # # # # #
        
        # store time and incidence into 1D vectors
        # beware that list length may vary => crop to rectangular array
        listSize=9999999
        for element in antennaPatternSlantRangeTime:
            if len(element) < listSize:
                listSize = len(element)
        slantRange=np.zeros(shape=(len(antennaPatternSlantRangeTime),listSize),dtype="float")
        incidenceAngle=np.zeros(shape=(len(antennaPatternSlantRangeTime),listSize),dtype="float")
        for i, element in enumerate(antennaPatternSlantRangeTime):
            slantRange[i,:] = antennaPatternSlantRangeTime[i][0:listSize]
            incidenceAngle[i,:] = antennaPatternIncidenceAngle[i][0:listSize]
        slantRange=slantRange.ravel()
        incidenceAngle=incidenceAngle.ravel()
        
        # near range
        slantRangeMin=min(slantRange)
        #print(slantRangeMin)
        
        # fit 2nd order polynomial
        #print(slantRange-slantRangeMin,incidenceAngle)
        poly_incidence = np.polyfit(slantRange-slantRangeMin,incidenceAngle, 2)
        #print(poly_incidence[0],poly_incidence[1],poly_incidence[2])
        
        ## Output incidence (for tests)
        #f = open('TestIncidence.txt', 'w')
        #for lineNumber in range(0,len(slantRange)):
        #        f.write("%e %f\n" % (slantRange[lineNumber]-slantRangeMin,incidenceAngle[lineNumber]))
        #f.close()
        
        if return_geoloc_corners:
            ## Find burst corner coordinates (lon, lat) using geolocation grid
            logger.info("Extracting coordinates of burst corners")
            lenBurst=len(burstAzimuthTime)
            geolocationGridRadar = np.vstack(([x*(C/2.0) for x in geolocationSlantRangeTime], [x.timestamp() for x in geolocationTime])).T
            ## Interpolation in 2D, in radar coordinates
            # Longitude
            myInterpolatorNNLon = NearestNDInterpolator(geolocationGridRadar, geolocationLongitude)
            myInterpolatorLinLon = LinearNDInterpolator(geolocationGridRadar, geolocationLongitude)
            # Latitude
            myInterpolatorNNLat = NearestNDInterpolator(geolocationGridRadar, geolocationLatitude)
            myInterpolatorLinLat = LinearNDInterpolator(geolocationGridRadar, geolocationLatitude)
    
            ## Loop over bursts
            burstCorners = np.zeros(shape=(lenBurst,10))
            for i in range(lenBurst):
                logger.debug(" Burst %d/%d" %(i+1, lenBurst))
                burstNearRangeSample = np.array([x for x in burstFirstValidSample[i] if x != '-1'], dtype=float).min()
                burstFarRangeSample = np.array([x for x in burstLastValidSample[i] if x != '-1'], dtype=float).max()
                burstBeg = (burstAzimuthTime[i]).timestamp()
                burstEnd = (burstAzimuthTime[i]+datetime.timedelta(seconds=burstVals["linesPerBurst"]/vals['PRF'])).timestamp()
                burstNearRange = vals["STARTING_RANGE"] + burstNearRangeSample/vals['RANGE_SAMPLING_FREQUENCY']*C/2
                burstFarRange = vals["STARTING_RANGE"] + burstFarRangeSample/vals['RANGE_SAMPLING_FREQUENCY']*C/2

                # First try with a linear interpolation
                myInterpolatorLon = myInterpolatorLinLon
                myInterpolatorLat = myInterpolatorLinLat
                burstUL_lon = myInterpolatorLon(burstNearRange, burstBeg)
                burstUL_lat = myInterpolatorLat(burstNearRange, burstBeg)
                burstUR_lon = myInterpolatorLon(burstNearRange, burstEnd)
                burstUR_lat = myInterpolatorLat(burstNearRange, burstEnd)
                burstLL_lon = myInterpolatorLon(burstFarRange, burstBeg)
                burstLL_lat = myInterpolatorLat(burstFarRange, burstBeg)
                burstLR_lon = myInterpolatorLon(burstFarRange, burstEnd)
                burstLR_lat = myInterpolatorLat(burstFarRange, burstEnd)

                burstCorners[i,:] = [burstAzimuthTimeSeconds[i], burstAzimuthAnxTime[i], burstUL_lon, burstUL_lat, burstUR_lon, burstUR_lat, burstLR_lon, burstLR_lat, burstLL_lon, burstLL_lat]

                # If linear interpolation fails, use near-neighbor instead
                if np.isnan(burstCorners[i,:]).any():
                    myInterpolatorLon = myInterpolatorNNLon
                    myInterpolatorLat = myInterpolatorNNLat
                    burstUL_lon = myInterpolatorLon(burstNearRange, burstBeg)
                    burstUL_lat = myInterpolatorLat(burstNearRange, burstBeg)
                    burstUR_lon = myInterpolatorLon(burstNearRange, burstEnd)
                    burstUR_lat = myInterpolatorLat(burstNearRange, burstEnd)
                    burstLL_lon = myInterpolatorLon(burstFarRange, burstBeg)
                    burstLL_lat = myInterpolatorLat(burstFarRange, burstBeg)
                    burstLR_lon = myInterpolatorLon(burstFarRange, burstEnd)
                    burstLR_lat = myInterpolatorLat(burstFarRange, burstEnd)

                    burstCorners[i,:] = [burstAzimuthTimeSeconds[i], burstAzimuthAnxTime[i], burstUL_lon, burstUL_lat, burstUR_lon, burstUR_lat, burstLR_lon, burstLR_lat, burstLL_lon, burstLL_lat]

                logger.debug("  burstNearRangeSample:", burstNearRangeSample, "burstFarRangeSample", burstFarRangeSample)
                logger.debug("  burstBeg:", burstBeg, " / burstEnd:", burstEnd)
                logger.debug("  burstNearRange:", burstNearRange, " / burstFarRange:", burstFarRange)
                logger.debug("  corner UL:",burstUL_lon, burstUL_lat," / corner UR :", burstUR_lon, burstUR_lat)
                logger.debug("  corner LL:",burstLL_lon, burstLL_lat," / corner LR :", burstLR_lon, burstLR_lat)


            df_geoloc_corners = pd.DataFrame(burstCorners, columns=['burstAzimuthTimeSeconds', 'burstAzimuthAnxTime', 'burstUL_lon', 'burstUL_lat', 'burstUR_lon', 'burstUR_lat', 'burstLR_lon', 'burstLR_lat', 'burstLL_lon', 'burstLL_lat'], dtype='float') 
        else:
            df_geoloc_corners = None
    
    
    
    #            for geolocationGridPoint in t.findall("geolocationGrid/geolocationGridPointList/geolocationGridPoint"):
    #            geolocationTime.append(datetime.datetime.strptime(geolocationGridPoint.find('azimuthTime').text, "%Y-%m-%dT%H:%M:%S.%f" ))
    #    #            geolocationSlantRangeTime.append((geolocationGridPoint.find('slantRangeTime').text).split())
    #            geolocationSlantRangeTime.append(float(geolocationGridPoint.find('slantRangeTime').text))
    #    #            geolocationLatitude.append(float(geolocationGridPoint.find('latitude')))
    #            geolocationLatitude.append(float(geolocationGridPoint.find('latitude').text))
    #            geolocationLongitude.append(float(geolocationGridPoint.find('longitude').text))
    #            geolocationIncidence.append(float(geolocationGridPoint.find('incidenceAngle').text))
    
        
        
        
        # # # # # # # # # #
        # Determine azimuth and range time of target
        # # # # # # # # # #
        
        if ( target_lon is not None and target_lat is not None ):
            logger.debug("Trying to find the time of closest approach...")
        
            # 3D coordinates of target on the ground
            [target_x, target_y, target_z] =  nsb_utils.lla2ecef(target_lat,target_lon,target_alt)
            logger.debug('Target coordinates (geographic): lon=%f , lat=%f , alt=%f' % (target_lon, target_lat, target_alt))
            logger.debug('Target coordinates (3D geocentric): x=%f , y=%f , z=%f' % (target_x, target_y, target_z))
            logger.debug('Earth radius = %.2f' % (np.sqrt(target_x**2 + target_y**2 +target_z**2)))
        
            # Grossly find state vector around closest approach (helps initializing optimizing)
            dist_sv_wrt_target = []
            for myt, myx, myy, myz in zip(orbTimeSeconds, orbPosX, orbPosY, orbPosZ):
                myd = nsb_utils.distance_3D(target_x, target_y, target_z, myx, myy, myz)
                dist_sv_wrt_target.append(myd)
            index_sv_min_approach = np.argmin(dist_sv_wrt_target)
            logger.debug("Closest state vector : ",index_sv_min_approach, min(dist_sv_wrt_target), orbTimeSeconds[index_sv_min_approach])
        
            # Find minimum distance by mimimizing satellite-to-ground distance (uses hermite interpolation routine)
            res = nsb_utils.find_min_distance(orbTimeSeconds[index_sv_min_approach], target_x, target_y, target_z, orbTimeSeconds, orbPosX, orbPosY, orbPosZ, orbVelX, orbVelY, orbVelZ)
            best_time_interp_hermite = res.x
            best_distance_interp_hermite = res.fun
            logger.debug("Time of closest approach : %f" % best_time_interp_hermite)
            logger.debug("Distance of closest approach : %f" % best_distance_interp_hermite)
        
            # Calculate satellite position at point of closest approach (uses hermite interpolation routine)
            best_sat_xp_interp_hermit = nsb_utils.interpolate_orbit_hermite(orbTimeSeconds, orbPosX, orbVelX, 6, best_time_interp_hermite)
            best_sat_yp_interp_hermit = nsb_utils.interpolate_orbit_hermite(orbTimeSeconds, orbPosY, orbVelY, 6, best_time_interp_hermite)
            best_sat_zp_interp_hermit = nsb_utils.interpolate_orbit_hermite(orbTimeSeconds, orbPosZ, orbVelZ, 6, best_time_interp_hermite)
            logger.debug("Satellite coordinates (3D geocentric): x=%f , y=%f , z=%f" % (best_sat_xp_interp_hermit, best_sat_yp_interp_hermit, best_sat_zp_interp_hermit))
        
            [best_sat_lat_interp_hermit, best_sat_lon_interp_hermit, best_sat_alt_interp_hermit] = nsb_utils.ecef2lla(best_sat_xp_interp_hermit, best_sat_yp_interp_hermit, best_sat_zp_interp_hermit)
            logger.debug("Satellite coordinates (geographic): lon=%f , lat=%f , alt=%f" % (best_sat_lon_interp_hermit, best_sat_lat_interp_hermit, best_sat_alt_interp_hermit))
        
            # Sanity check
            best_distance_interp_hermite_check = nsb_utils.distance_3D(target_x, target_y, target_z, best_sat_xp_interp_hermit, best_sat_yp_interp_hermit, best_sat_zp_interp_hermit)
        
            tolerance_distance = 1e-12 # Maximum distance tolerated between optimization function output and re-calculated 3D distance 
            if ( np.abs(best_distance_interp_hermite_check - best_distance_interp_hermite) > tolerance_distance ):
                logger.warning("Warning : inconsistent 3D distance returned by optimize...")
        
            # Calculate the size of the crop, in number of rows / columns
            crop_columns = crop_lon_km/pixel_ratio*1000/orbVelocity[0]*vals["PRF"]/np.cos(15./180.*np.pi) # convert NS size of crop in km to width in columns (grossly account for satellite heading)
            crop_rows = crop_lat_km*1000/C*vals["RANGE_SAMPLING_FREQUENCY"]*np.sin(incidenceAngle[0]/180.*np.pi)/np.cos(15./180.*np.pi) # convert EW size of crop in km to height in rows (grossly account for satellite heading)
            logger.info("Crop size : rows = %f / cols = %f" % (crop_rows, crop_columns))
        
        # # # # # # # # # # # # # # # # #
        # # PREPARE OUTPUT FILE NAMES # #
        # # # # # # # # # # # # # # # # #
        
        # Use prefix provided by user.
        try:
            prefix
        except: # Otherwhise create default prefix of the form "20150910_iw3_vv"
            prefix = str(valsMerge["DATE"]) + '_' + mode + '_' + polarization
        
        svFile = os.path.join(outdir, 'hdr_data_points_' + prefix + '.rsc')
        slcoutFilename = os.path.join(outdir, prefix + '.slc')
        rscoutFileName = os.path.join(outdir, prefix + '.raw' + '.rsc')
        dcDataFile = os.path.join(outdir, prefix + '_dopCentDataPolynom' + '.txt')
        dcGeomFile = os.path.join(outdir, prefix + '_dopCentGeomPolynom' + '.txt')
        azFMFile = os.path.join(outdir, prefix + '_azFM' + '.txt')
        burstFile = os.path.join(outdir, prefix + '_burst' + '.txt')
        validFile = os.path.join(outdir, prefix + '_valid' + '.txt')
        incidenceFile = os.path.join(outdir, prefix + '_incidence' + '.txt')
        geolocationFile = os.path.join(outdir, prefix + '_geolocation' + '.txt')
        paramFile = os.path.join(outdir, prefix + '_param' + '.rsc')
        if ( target_lon is not None and target_lat is not None ):
            targetFile = os.path.join(outdir, prefix + '_target' + '.rsc')
        
        burstVals["ROOTNAME"] = prefix
        burstVals["INDIR"] = os.path.join(os.getcwd(),safepath[0],"measurement")
        burstVals["INFILETIF"] =  os.path.basename(measurementPath[0])
        burstVals["OUTDIR"] = os.getcwd()
        burstVals["OUTFILESLC"] = slcoutFilename
        burstVals["SETLAGTOZERO"] = 'yes'
        
        logger.debug("Date : %s" % str(valsMerge["DATE"]))
        logger.debug("Mode : %s" % mode)
        logger.debug("Pola : %s" % polarization)
        logger.debug("First line : %s" % str(firstLineTime))
        logger.debug("Last line  : %s" % str(lastLineTime))
        
        # # # # # # # # # # # # # # # # # #
        # 10. Save state vectors to file
        lenOrbit=len(orbTimeSecondsConcat)
        logger.debug("Number of State Vectors : %s" % str(lenOrbit))
        if write_output:
            f = open(svFile, 'w')
            for orbitNumber in range(0,lenOrbit):
                f.write("%-14.6f %-14.6f %-14.6f %-14.6f %-14.6f %-14.6f %-14.6f\n" % (float(orbTimeSecondsConcat[orbitNumber]), float(orbPosXConcat[orbitNumber]), float(orbPosYConcat[orbitNumber]), float(orbPosZConcat[orbitNumber]), float(orbVelXConcat[orbitNumber]), float(orbVelYConcat[orbitNumber]), float(orbVelZConcat[orbitNumber])))
            f.close()
    #    else:
    #        df_orbits = pd.DataFrame(np.hstack((orbTimeSecondsConcat, orbPosXConcat, orbPosYConcat, orbPosZConcat, orbVelXConcat, orbVelYConcat, orbVelZConcat)))
        
        
        # # # # # # # # # # # # # # # # # #
        # 11. Save RSC file
        if write_output:
            if writeRsc:
                f = open(rscoutFileName, 'w')
                for k, v in valsMerge.items():
                    logger.debug("{:40s} {}".format(k, v))
                    f.write("%-40s %-30s\n" % (k, v))
                f.close()
        
        # # # # # # # # # # # # # # # # # #
        # 12. Save Doppler Centroid info to file
        lenDC=len(dcAzimuthTime)
        logger.debug("Number of Doppler Centroid Polynomials : %s" % str(lenDC))
        if write_output:
            f = open(dcDataFile, 'w')
            for dcNumber in range(0,lenDC):
                f.write("%-15s %-15s %-15s %-15s %-15s %-15s %-15s\n" % (dcAzimuthTimeSeconds[dcNumber], dcT0[dcNumber],  dataDcPolynomialOrder0[dcNumber],  dataDcPolynomialOrder1[dcNumber],  dataDcPolynomialOrder2[dcNumber],  dataDcPolynomialOrder3[dcNumber],  dataDcPolynomialOrder4[dcNumber] ))
            f.close()
            f = open(dcGeomFile, 'w')
            for dcNumber in range(0,lenDC):
                f.write("%-15s %-15s %-15s %-15s %-15s %-15s %-15s\n" % (dcAzimuthTimeSeconds[dcNumber], dcT0[dcNumber],  geometryDcPolynomialOrder0[dcNumber],  geometryDcPolynomialOrder1[dcNumber],  geometryDcPolynomialOrder2[dcNumber],  geometryDcPolynomialOrder3[dcNumber],  geometryDcPolynomialOrder4[dcNumber] ))
            f.close()
        
        
        # # # # # # # # # # # # # # # # # #
        # 13. Save Azimuth FM rate info to file
        lenAzFM=len(azFMRateTime)
        logger.debug("Number of Azimuth FM Rate estimations : %s" % str(lenAzFM))
        if write_output:
            f = open(azFMFile, 'w')
            for azFMNumber in range(0,lenAzFM):
                f.write("%-15s %-15s %-15s %-15s %-15s\n" % (azFMRateTimeSeconds[azFMNumber], azFMRatet0[azFMNumber], azFMRatec0[azFMNumber], azFMRatec1[azFMNumber], azFMRatec2[azFMNumber]))
            f.close()
        
        
        # # # # # # # # # # # # # # # # # #
        # 14. Save general burst information
        lenBurst=len(burstAzimuthTime)
        logger.debug("\rNumber of bursts : %s\n" % str(lenBurst))
        if write_output:
            f = open(burstFile, 'w')
            for BurstNumber in range(0,lenBurst):
                f.write("%-80s %-15s %-15s %-15s %-15s %-15s\n" % (burstSafedir[BurstNumber], burstAzimuthTimeSeconds[BurstNumber], burstAzimuthAnxTime[BurstNumber], burstLongitude[BurstNumber], burstLatitude[BurstNumber], burstByteOffset[BurstNumber] ))
            f.close()
        else:
            df_bursts = pd.concat([pd.DataFrame(burstSafedir, columns=['burstSafedir'], dtype='str'), pd.DataFrame(np.vstack((burstAzimuthTimeSeconds, burstAzimuthAnxTime, burstLongitude, burstLatitude, burstByteOffset)).T, columns=['burstAzimuthTimeSeconds', 'burstAzimuthAnxTime', 'burstLongitude', 'burstLatitude', 'burstByteOffset'], dtype='float')], axis=1)
        
        # # # # # # # # # # # # # # # # # #
        # 15. Save burst first / last valid sample
        if write_output:
            f = open(validFile, 'w')
            for BurstNumber in range(0,lenBurst):
                for LineNumber in range(0,int(burstVals["linesPerBurst"])):
                    f.write("%-6d" % int(burstFirstValidSample[BurstNumber][LineNumber]))
                f.write("\n")
                for LineNumber in range(0,int(burstVals["linesPerBurst"])):
                    f.write("%-6d" % int(burstLastValidSample[BurstNumber][LineNumber]))
                f.write("\n")
            f.close()
        
        
        # # # # # # # # # # # # # # # # # #
        # 16. Save parameters information
        if write_output:
            f = open(paramFile, 'w')
            for k, v in burstVals.items():
                # print("{:-40s} {:-30s}".format(k, v))
                f.write("%-40s %-30s\n" % (k, v))
            f.close()
        
        
        # # # # # # # # # # # # # # # # # #
        # 17. Save incidence angle polynomial
        if write_output:
            f = open(incidenceFile, 'w')
            f.write("%e %e %e %e\n" % (slantRangeMin,poly_incidence[2],poly_incidence[1],poly_incidence[0]))
            f.close()
        
        
        # # # # # # # # # # # # # # # # # #
        # 18. Save geolocation information
        lenGeolocation=len(geolocationTime)
        if write_output:
            f = open(geolocationFile, 'w')
            for gcp in range(0,lenGeolocation):
                f.write("%-30s %-15s %-15s %-15s %-15s %-15s\n" % (geolocationTime[gcp], geolocationTimeSeconds[gcp],
                        geolocationLatitude[gcp], geolocationLongitude[gcp],
                geolocationRange[gcp], geolocationIncidence[gcp]))
            f.close()
        else:
            df_geoloc = pd.DataFrame(np.vstack((geolocationTime, geolocationTimeSeconds, geolocationLatitude, geolocationLongitude, geolocationRange, geolocationIncidence)).T, columns=['geolocationTime', 'geolocationTimeSeconds', 'geolocationLatitude', 'geolocationLongitude', 'geolocationRange', 'geolocationIncidence'])
        
        # # # # # # # # # # # # # # # # # #
        # 19. Save target coordinates
        if write_output:
            if ( target_lon is not None and target_lat is not None ):
                f = open(targetFile, 'w')
                f.write("TARGET_LATITUDE                 %.5f\n" % target_lat)
                f.write("TARGET_LONGITUDE                %.5f\n" % target_lon)
                f.write("TARGET_ALTITUDE                 %.5f\n" % target_alt)
                f.write("TARGET_ORBITTIME                %.9f\n" % best_time_interp_hermite)
                f.write("TARGET_RANGEDIST                %.9f\n" % best_distance_interp_hermite)
                f.write("TARGET_ORBITLAT                 %.5f\n" % best_sat_lat_interp_hermit)
                f.write("TARGET_ORBITLON                 %.5f\n" % best_sat_lon_interp_hermit)
                f.write("TARGET_ORBITALT                 %.5f\n" % best_sat_alt_interp_hermit)
                f.write("TARGET_CROP_COL                 %.5f\n" % crop_columns)
                f.write("TARGET_CROP_ROW                 %.5f\n" % crop_rows)
                f.close()
        
        if not(write_output):
            return valsMerge, df_bursts, df_geoloc, df_geoloc_corners
        

    # From nsb_prep_slc_latcut_s1_new.py

    # # # # # # # # # # # # # # # #
    # # parse SAFE name to get some basic info

    def safenameParse(self, SAFEname):
        sardir = os.path.basename(SAFEname)
        sensor = sardir.split('_')[0]
        acquisitionMode = sardir.split('_')[1]
        processingLevel = sardir.split('_')[2]
        polarisationMode = sardir.split('_')[4]
        acquisitionStartDate = sardir.split('_')[5]
        acquisitionStopDate = sardir.split('_')[6]
        return [sensor, acquisitionMode, processingLevel, polarisationMode,
                acquisitionStartDate, acquisitionStopDate]

    # # # # # # # # # # # # # # # #
    # # automatic burst selection

    def auto_select_burst(self, exponent_scaling=0.9, start_AnxTime=-999, stop_AnxTime=-999, return_geoloc_corners=False, verbose=0): 

        logger = nsb_utils.change_log_level(verbose ,[nsb_io, nsb_procparser, nsb_utils])

        # Start / Stop time, if any
        if (start_AnxTime != -999 and stop_AnxTime != -999) :
            # Restrict analysis to only the bursts between these bounds
            start_AnxTime_prior = start_AnxTime
            stop_AnxTime_prior  = stop_AnxTime
        else:
            # No priors, analyze all the bursts
            start_AnxTime_prior = -1*np.inf
            stop_AnxTime_prior  =    np.inf
        logger.info("Auto : prior start_AnxTime = %s / stop_AnxTime = %s" % (start_AnxTime_prior, stop_AnxTime_prior))

        # Sar directories
        SarDirs = list(self.verif_all.keys())
        # Convert to date
        SarDates = []
        for Dir in SarDirs:
            SarDates.append(datetime.datetime.strptime(Dir, '%Y%m%d'))
        # Ascending node crossing time for each burst
        AnxTimes = self.verif_all.values()
        
        # Calculate time difference between bursts
        AnxTimes_mediandiff = []
        for AnxTimeTmp in AnxTimes:
            # Only keep products with > 1 burst and skip gaps
            if len(AnxTimeTmp) > 1 and not((np.abs(np.diff(AnxTimeTmp))>10).any()):
                AnxTimes_mediandiff.append(np.nanmedian(np.diff(AnxTimeTmp)))
        AnxTimes_mediandiff_all = np.nanmedian(AnxTimes_mediandiff)
        logger.info("Median time difference between two bursts : %f seconds" % (AnxTimes_mediandiff_all))
        AnxTimes_tol_fraction = 0.1 # Tolerance fraction : time difference between bin center and anx time must not exceed this fraction of burst duration
        AnxTimes_tol_sec = AnxTimes_mediandiff_all*AnxTimes_tol_fraction # Tolerance
        logger.info("Tolerance : %f seconds" % (AnxTimes_tol_sec))
        
        # Flatten list of ANX times for all bursts
        AnxTimes_concat = [item for sublist in AnxTimes for item in sublist]
        logger.info("AnxTimes : min = %f / max = %f" % (np.min(AnxTimes_concat), np.max(AnxTimes_concat)))
        # Define bins
        bins = np.linspace(np.min(AnxTimes_concat)-AnxTimes_mediandiff_all/2., np.max(AnxTimes_concat)+AnxTimes_mediandiff_all/2., round((np.max(AnxTimes_concat)- np.min(AnxTimes_concat))/AnxTimes_mediandiff_all)+2)
        #print(" bins: ", len(bins), bins)
        
        # Count occurrences of ANX times in each bin
        counts, bin_edges = np.histogram(AnxTimes_concat, bins=bins)
        
        # Digitize according to spacing between bursts
        index_bins = np.digitize(AnxTimes_concat, bins=bin_edges)
        #print("AnxTimes_concat", len(AnxTimes_concat), AnxTimes_concat)
        #print("index_bins:", np.min(index_bins), np.max(index_bins), len(index_bins), index_bins)
        
        # Refine the bin centers
        bin_centers = []
        burst_global_index = np.arange(np.min(index_bins), np.max(index_bins)+1) # BUG ??
        #print(burst_global_index.shape, burst_global_index)
        #burst_global_index = np.arange(0, len(bins))
        #print(burst_global_index.shape, burst_global_index)
        for i in burst_global_index:
            bin_centers.append(np.mean([AnxTimes_concat[x] for x in list(np.where(index_bins==i)[0])]))
        bin_centers = np.array(bin_centers)
        #print("burst_global_index", len(burst_global_index), burst_global_index)
        #print("bin_centers", len(bin_centers), bin_centers)
        
        self.AnxTimes_mean_bin = bin_centers
        
        ## Make list of AnxTimes
        #tolerance_burst_time=0.1
        #global_AnxTimes = np.ones(len(self.AnxTimes_mean_bin))*np.nan
        #for sarDir in self.burstCatalogue.keys():
        #    if not(np.isnan(global_AnxTimes).any()):
        #        break
        #    for i, AnxTime in enumerate(self.AnxTimes_mean_bin):
        #        bestBurst = (np.where(np.abs(self.burstCatalogue[sarDir]['burstAzimuthAnxTime'] - AnxTime) < tolerance_burst_time))
        #        if(len(bestBurst[0])>0):
        #            global_AnxTimes[i] = self.AnxTimes_mean_bin[i]
        #            continue
                                        
        # Make list of AnxTimes
        tolerance_burst_time=0.1
        global_AnxTimes = np.ones(len(self.AnxTimes_mean_bin))*np.nan
        if return_geoloc_corners:
            global_burstCorners = np.ones([len(self.AnxTimes_mean_bin), 10])*np.nan
        else:
            global_burstCorners = None
        for sarDir in self.burstCatalogue.keys():
            if not(np.isnan(global_AnxTimes).any()):
                break
            if return_geoloc_corners:
                junka, junkb, junkc, mygeoloccorners = self.safe2rsc(sarDir, mode=self.swath, polarization=self.polarization, verbose=False, write_output=False, return_geoloc_corners=True)
            for i, AnxTime in enumerate(self.AnxTimes_mean_bin):
                bestBurst = (np.where(np.abs(self.burstCatalogue[sarDir]['burstAzimuthAnxTime'] - AnxTime) < tolerance_burst_time))
                if(len(bestBurst[0])>0):
                    global_AnxTimes[i] = self.AnxTimes_mean_bin[i]
                    if return_geoloc_corners:
                        global_burstCorners[i,:] = mygeoloccorners.iloc[[int(bestBurst[0])]].values
                    continue
                                        
        #
        # # Plot location of bursts / burst boundaries
        # plt.figure(figsize=(13,7))
        # plt.plot(AnxTimes_concat, np.array(AnxTimes_concat)*0.+2, 'or')
        # plt.plot(bins, bins*0.+2, 'ob')
        # plt.plot(bin_centers, bin_centers*0.+2, 'ok', markersize=2, zorder=10)
        # plt.plot(bin_edges, bin_edges*0.+2, 'o', color='white', markersize=2, zorder=10)
        # plt.show()
        #
        # # Plot burst ANX time for all dates, as in plot_time_lat
        # plt.figure()
        # plt.plot(index_bins,'o')
        # plt.show()
        #
        #
        # # Plot histrogram of ANX times
        # plt.figure(figsize=(13,7))
        # plt.hist(bin_edges[:-1], bin_edges, weights=counts)
        # plt.ylim(np.min(counts)-10,np.max(counts)+10)
        # plt.show()
        
        tolerance_AnxTime = 6.0 # Tolerance to mask out values outside bounds ( 3 seconds = approx 1 bursts)

        # Populate the array of bursts in two dimensions: date VS ANX time
        array_of_burst = np.zeros([len(bin_centers), len(SarDates)])
        array_of_burst_times = np.zeros([len(bin_centers), len(SarDates)])
        for j, Anx_list in enumerate(AnxTimes):
            for i, Anx_time in enumerate(bin_centers):
                if ( Anx_time > start_AnxTime_prior-tolerance_AnxTime and Anx_time < stop_AnxTime_prior+tolerance_AnxTime):
                    array_of_burst[i, j] = sum(map(lambda x : np.abs(x-bin_centers[i])<AnxTimes_tol_sec, Anx_list))
                    array_of_burst_times[i, j] = sum(map(lambda x : x*(np.abs(x-bin_centers[i])<AnxTimes_tol_sec), Anx_list))
        globalBursts = pd.DataFrame(array_of_burst_times.copy())
        
#        # Make list of burst corners
#        tolerance_burst_time=0.1
#        global_burst_corners = np.zeros(len(s.AnxTimes_mean_bin), 4)*np.nan
#        for sarDir in self.burstCatalogue.keys():
#            if not(np.isnan(global_AnxTimes).any()):
#                break
#            else:
#                myrsc, myburstpd, mygeolocpd, mygeoloccornerspd = self.safe2rsc(sarDir, self.swath, self.polarization, True, False)
#            for i, AnxTime in enumerate(mygeoloccornerspd):
#                bestBurst = (np.where(np.abs(self.burstCatalogue[sarDir]['burstAzimuthAnxTime'] - AnxTime) < tolerance_burst_time))
#                if(len(bestBurst[0])>0):
#                    global_AnxTimes[i] = self.AnxTimes_mean_bin[i]
#                    continue

        ########################################################################
        #
        ###### SHOULD NOT BE NECESSARY => THIS COMES FROM A PREVIOUS BUG
        #
        # Replace those with a 2 by a 1
        if len(array_of_burst[array_of_burst >1])>0:
            logger.warning(" Warning : multiple files found!")
            multiple_bursts=list((np.where(array_of_burst >1)))
            logger.debug(multiple_bursts[0])
            for i, index_multiple_burst in enumerate(multiple_bursts[0]):
                logger.debug(SarDirs[multiple_bursts[1][i]], multiple_bursts[0][i])
        array_of_burst[array_of_burst ==2] = 1
        #
        #
        ########################################################################
        
        #
        # # Display matrix
        # plt.figure()
        # plt.matshow(array_of_burst, origin='lower')
        # plt.colorbar()
        # plt.show()
        
        # Maximum number of bursts that can be selected
        max_burst = array_of_burst.shape[0]
        
        select_rectangle = np.zeros([max_burst, max_burst])
        for num_select_burst in np.arange(max_burst) + 1: # Loop over number of selected bursts
            for offset_burst in np.arange(max_burst - num_select_burst + 1): # Offset the selection
                index_select = offset_burst+np.arange(num_select_burst) # Selected indexes based on number of bursts and offset
                # Count the number of images that have valid bursts for all these indexes
                select_rectangle[num_select_burst-1, offset_burst] = array_of_burst[index_select,:].prod(axis=0).sum()
        
        # # Display matrix
        # plt.figure()
        # plt.matshow(select_rectangle, origin='lower')
        # plt.colorbar()
        # plt.show()
        
        x = np.linspace(0, 1, select_rectangle.shape[0])
        y = np.linspace(0, 1, select_rectangle.shape[1])
        xv, yv = np.meshgrid(x, y)
        
        # # Display matrix
        # plt.figure()
        # plt.matshow(xv, origin='lower')
        # plt.colorbar()
        # plt.show()
        
        # # Display matrix
        # plt.figure()
        # plt.matshow(yv**exponent_scaling*select_rectangle, origin='lower')
        # plt.colorbar()
        # plt.show()
        
        # Find maximum of array
        max_index = np.where(yv**exponent_scaling*select_rectangle == np.amax(yv**exponent_scaling*select_rectangle))
        num_select_burst = max_index[0].reshape(-1)[0]+1
        offset_burst = max_index[1].reshape(-1)[0]
        
        index_select = offset_burst + np.arange(num_select_burst) # Selected indexes based on number of bursts and offset
        select_rectangle_best = array_of_burst[index_select,:]  # The burst list
        select_rectangle_best_index = list(np.where(select_rectangle_best.prod(axis=0))[0]) # Images with bursts that are all valid
        #print(select_rectangle_best_index)
        
        logger.info("Automatic burst selection result: %d bursts / %d dates " % (num_select_burst, len(select_rectangle_best_index)))

        # Finally, the indexes of the rectangular time VS ANX time selection
        xv_select, yv_select = np.meshgrid(index_select, select_rectangle_best_index)
        #print(xv_select, yv_select)
        
        # Set a flag
        array_of_burst_select = array_of_burst.copy()
        array_of_burst_select[xv_select,yv_select] = 2
        
        
        # # Display matrix
        # plt.figure()
        # plt.matshow(array_of_burst, origin='lower')
        # plt.colorbar()
        # plt.show()
        #
        # # Display matrix
        # plt.figure()
        # plt.matshow(array_of_burst_select, origin='lower')
        # plt.colorbar()
        # plt.show()
        
        # List of selected dates
        logger.info("  => List of selected dates: ",np.array(SarDirs)[list(select_rectangle_best_index)])
        
        # List of bursts
        logger.info("  => List of selected bursts ",bin_centers[index_select])
        
        ## Size of selected rectangle
        #print(len(np.array(SarDirs)[list(select_rectangle_best_index)]), len(bin_centers[index_select]))
                            
        return globalBursts, global_AnxTimes, array_of_burst, array_of_burst_select, bin_centers[index_select], np.array(SarDirs)[list(select_rectangle_best_index)], global_burstCorners
        
    # # # # # # # # # # # # # # # #
    # #  compute burst statitics


    def stat_burst(self, df_bursts, start_lat, stop_lat, start_AnxTime,
                   stop_AnxTime, refine, verbose):

        logger = nsb_utils.change_log_level(verbose ,[nsb_io, nsb_procparser, nsb_utils])

        burstAzimuthAnxTime = []
        burstLongitude = []
        burstLatitude = []

        logger.info("Analysing burst statistics with priors:")
        logger.info(" start_lat : %-11.3f" % start_lat)
        logger.info(" stop_lat  : %-11.3f" % stop_lat)
        logger.info(" start_AnxTime : %-11.3f" % start_AnxTime)
        logger.info(" stop_AnxTime  : %-11.3f" % stop_AnxTime)

        # read list of bursts (all bursts, all images)
        for sarDir in df_bursts.keys():
            logger.debug("Reading bursts in %s" %sarDir)
            burstAzimuthAnxTime.append(df_bursts[sarDir]['burstAzimuthAnxTime'])
            burstLongitude.append(df_bursts[sarDir]['burstLongitude'])
            burstLatitude.append(df_bursts[sarDir]['burstLatitude'])

        # flatten lists
        def flatten(t):
            return [item for sublist in t for item in sublist]
        burstAzimuthAnxTime = flatten(burstAzimuthAnxTime)
        burstLongitude = flatten(burstLongitude)
        burstLatitude = flatten(burstLatitude)
        logger.info("Analysing %d bursts in %d acquisitions " %(len(burstAzimuthAnxTime), len(df_bursts.keys())))

        # sort list
        ListSorted = sorted(zip(burstAzimuthAnxTime, burstLatitude))
        burstLatitudeSorted = [ListSorted[i][1] for i in range(len(ListSorted))]
        burstAzimuthAnxTimeSorted = [ListSorted[i][0]
                                     for i in range(len(ListSorted))]
        meanAzimuthAnxBurst = 2.758
        minAzimuthAnxTime = burstAzimuthAnxTimeSorted[0]-meanAzimuthAnxBurst*0.5
        maxAzimuthAnxTime = burstAzimuthAnxTimeSorted[-1]+meanAzimuthAnxBurst*0.5
        nbin = int(round((maxAzimuthAnxTime-minAzimuthAnxTime) //
                         meanAzimuthAnxBurst))
        AzimuthAnxTimeHist = [0 for i in range(nbin)]
        AzimuthAnxTimeMean = [0. for i in range(nbin)]
        burstLatitudeMean = [0. for i in range(nbin)]
        k = 0
        #f = open(fileout, 'w')
        logger.debug("Burst histogram:")
        for i in range(nbin):
            binlim = minAzimuthAnxTime + (i+1)*meanAzimuthAnxBurst
            while (k < len(burstAzimuthAnxTimeSorted) and
                   burstAzimuthAnxTimeSorted[k] < binlim):
                AzimuthAnxTimeHist[i] = AzimuthAnxTimeHist[i]+1
                AzimuthAnxTimeMean[i] = (AzimuthAnxTimeMean[i] +
                                         burstAzimuthAnxTimeSorted[k])
                burstLatitudeMean[i] = burstLatitudeMean[i]+burstLatitudeSorted[k]
                k = k+1
            AzimuthAnxTimeMean[i] = (float)(AzimuthAnxTimeMean[i] /
                                            float(AzimuthAnxTimeHist[i]))
            burstLatitudeMean[i] = (float)(burstLatitudeMean[i] /
                                           float(AzimuthAnxTimeHist[i]))
        #    f.write("%-15s %-15s %-15s\n"
        #            % (AzimuthAnxTimeMean[i], AzimuthAnxTimeHist[i],
        #                burstLatitudeMean[i]))
            logger.debug("* %11.5f * %3d * %-12.3f *"
                    % (AzimuthAnxTimeMean[i], AzimuthAnxTimeHist[i],
                        burstLatitudeMean[i]))

            if abs(AzimuthAnxTimeMean[i]-start_AnxTime) < self.safetyMarginAnxTime:
                firstBurstKeep = i
            if abs(AzimuthAnxTimeMean[i]-stop_AnxTime) < self.safetyMarginAnxTime:
                lastBurstKeep = i
        AzimuthAnxTimeHistRestrict = AzimuthAnxTimeHist[firstBurstKeep:
                                                        lastBurstKeep]
        maxburst = max(AzimuthAnxTimeHistRestrict)

        logger.info("After first pass:")
        logger.info(" First and last burst kept : %d / %d" %(firstBurstKeep, lastBurstKeep))
        logger.info(" Number of images for these bursts : %d / %d" %(AzimuthAnxTimeHist[firstBurstKeep], AzimuthAnxTimeHist[lastBurstKeep]))
        logger.info(" Maximum number of images per burst : %d" %(maxburst))
        logger.info(" Start / Stop AnxTimes after analysis : %f / %f" %(start_AnxTime, stop_AnxTime))

        #f.write("First and last burst kept "+str(firstBurstKeep) +
        #        " "+str(lastBurstKeep)+"\n")
        #f.write("Number of images for these bursts " +
        #        str(AzimuthAnxTimeHist[firstBurstKeep]) +
        #        " "+str(AzimuthAnxTimeHist[lastBurstKeep])+"\n")
        #f.write("Maximum number of images per burst "+str(maxburst)+"\n")

        if refine:
            if AzimuthAnxTimeHist[firstBurstKeep] < (2*maxburst)//3:
                firstBurstKeep = firstBurstKeep+1
                if lastBurstKeep < len(AzimuthAnxTimeHist)-1:
                    lastBurstKeep = lastBurstKeep+1
                    if AzimuthAnxTimeHist[lastBurstKeep] < (2*maxburst)//3:
                        lastBurstKeep = lastBurstKeep-1
            if AzimuthAnxTimeHist[firstBurstKeep] < (2*maxburst)//3:
                firstBurstKeep = firstBurstKeep+1
                if lastBurstKeep < len(AzimuthAnxTimeHist)-1:
                    lastBurstKeep = lastBurstKeep+1
                    if AzimuthAnxTimeHist[lastBurstKeep] < (2*maxburst)//3:
                        lastBurstKeep = lastBurstKeep-1
            if AzimuthAnxTimeHist[firstBurstKeep] < (2*maxburst)//3:
                firstBurstKeep = firstBurstKeep+1
                if lastBurstKeep < len(AzimuthAnxTimeHist)-1:
                    lastBurstKeep = lastBurstKeep+1
                    if AzimuthAnxTimeHist[lastBurstKeep] < (2*maxburst)//3:
                        lastBurstKeep = lastBurstKeep-1
            if AzimuthAnxTimeHist[lastBurstKeep] < (2*maxburst)//3:
                lastBurstKeep = lastBurstKeep-1
                if firstBurstKeep > 0:
                    firstBurstKeep = firstBurstKeep - 1
                    if AzimuthAnxTimeHist[firstBurstKeep] < (2*maxburst)//3:
                        firstBurstKeep = firstBurstKeep+1
            if AzimuthAnxTimeHist[lastBurstKeep] < (2*maxburst)//3:
                lastBurstKeep = lastBurstKeep-1
                if firstBurstKeep > 0:
                    firstBurstKeep = firstBurstKeep - 1
                    if AzimuthAnxTimeHist[firstBurstKeep] < (2*maxburst)//3:
                        firstBurstKeep = firstBurstKeep+1
            if AzimuthAnxTimeHist[lastBurstKeep] < (2*maxburst)//3:
                lastBurstKeep = lastBurstKeep-1
                if firstBurstKeep > 0:
                    firstBurstKeep = firstBurstKeep - 1
                    if AzimuthAnxTimeHist[firstBurstKeep] < (2*maxburst)//3:
                        firstBurstKeep = firstBurstKeep+1
            start_AnxTime = AzimuthAnxTimeMean[firstBurstKeep]
            stop_AnxTime = AzimuthAnxTimeMean[lastBurstKeep]
        #    f.write("After refinement:")
        #    f.write("First and last burst kept "+str(firstBurstKeep) +
        #            " "+str(lastBurstKeep)+"\n")
        #    f.write("Number of images for these bursts " +
        #            str(AzimuthAnxTimeHist[firstBurstKeep]) +
        #            " "+str(AzimuthAnxTimeHist[lastBurstKeep])+"\n")
        #    f.write("Maximum number of images per burst "+str(maxburst)+"\n")
        #f.close()
            logger.info("After refinement:")
            logger.info(" First and last burst kept: %d / %d" %(firstBurstKeep, lastBurstKeep))
            logger.info(" Number of images for these bursts : %d / %d" %(AzimuthAnxTimeHist[firstBurstKeep], AzimuthAnxTimeHist[lastBurstKeep]))
            logger.info(" Maximum number of images per burst : %d"%(maxburst))
            logger.info(" Start / Stop AnxTimes after analysis : %f / %f" %(start_AnxTime, stop_AnxTime))

        return(start_AnxTime, stop_AnxTime)

    # # # # # # # # # # # # # # # #
    # # extract burst index corresponding to start / stop latitudes

    def lat_cut_s1(self, df_bursts, sardir, swath, polarization, start_lat, stop_lat,
                   start_AnxTime, stop_AnxTime, force, verbose):
        
        date = os.path.basename(sardir)

        logger = nsb_utils.change_log_level(verbose ,[nsb_io, nsb_procparser, nsb_utils])

        logger.info("Cutting %s between latitudes %f -> %f" %(sardir, start_lat, stop_lat))

        burstSafedir = df_bursts['burstSafedir'].values
        burstAzimuthTimeSeconds = df_bursts['burstAzimuthTimeSeconds'].values
        burstAzimuthAnxTime = df_bursts['burstAzimuthAnxTime'].values
        burstLongitude = df_bursts['burstLongitude'].values
        burstLatitude = df_bursts['burstLatitude'].values
        burstByteOffset = df_bursts['burstByteOffset'].values
        
        # array "burstLatitude" is supposed to be sorted in chronological order
        if burstLatitude[0] < burstLatitude[-1]:
            orbitDirection = "ascending"
        else:
            orbitDirection = "descending"
        logger.debug("Orbit direction : {}".format(orbitDirection))
        logger.debug("Burst latitudes : {}".format(burstLatitude))
        logger.debug("Burst AnxTime   : {}".format(burstAzimuthAnxTime))
        # start_lat has to be to the south of stop_lat
        if (start_lat > stop_lat):
            logger.debug("Reversing order of start_lat and stop_lat.")
            stop_lat_tmp = stop_lat
            stop_lat = start_lat
            start_lat = stop_lat_tmp
        # safety margin : start_lat and stop_lat must not be
        # too close from burst latitude
        if ((np.abs(np.asarray(burstLatitude) - start_lat) <
             self.safetyMarginLatitude).any()):
            start_lat = start_lat - self.safetyMarginLatitude
            logger.debug("Adjusting start_lat to {0:f}.".format(start_lat))
        if ((np.abs(np.asarray(burstLatitude) - stop_lat) <
             self.safetyMarginLatitude).any()):
            stop_lat = stop_lat + self.safetyMarginLatitude
            logger.debug("Adjusting stop_lat to {0:f}.".format(stop_lat))
        # find north / south indexes
        logger.debug("Start / stop latitudes : {0:f} / {1:f}"
                  .format(start_lat, stop_lat))

        # dlatburst is set to a value that must be larger than maximum burst
        # latitude spacing (at equator?), but lower than twice the latitude spacing
        dlatburst = 0.33
        logger.debug("Start / stop latitudes (with safety margin) :  {0:f} / {1:f}"
                .format(start_lat-dlatburst, stop_lat+dlatburst))
        # First selection based on latitude
        burstToKeep = [i for i in range(len(burstLatitude))
                       if burstLatitude[i] > start_lat - dlatburst and
                       burstLatitude[i] < stop_lat + dlatburst]
        logger.debug("Bursts to keep : {}".format(burstToKeep))

        # write selected bursts
        self.verif_all[sardir] = []
        #f = open(fileout, 'w')
        for BurstNumber in burstToKeep:
            logger.debug("XX %-80s %-15s %-15s %-15s %-15s\n"
                    % (burstSafedir[BurstNumber],
                       burstAzimuthTimeSeconds[BurstNumber],
                       burstAzimuthAnxTime[BurstNumber],
                       burstLatitude[BurstNumber],
                       burstByteOffset[BurstNumber]))
            self.verif_all[sardir].append(burstAzimuthAnxTime[BurstNumber])

        if len(burstToKeep) == 0:
            safeDirList = list(set(burstSafedir))
            if force:
                logger.info("Acquisition %s entirely outside of requested start / stop latitude coverage!" % (date))
            else:
                logger.info("Acquisition %s entirely outside of requested start / stop Anx time coverage!" % (date))
            for safeDir in safeDirList:
                logger.info(" Unused SAFE (out of range): %s" % safeDir)
            return (start_AnxTime, stop_AnxTime)

        # Find burst ANX time for any burst that falls very close to the start / stop latitudes
        # Useful to make a first blind guess of start_AnxTime / stop_AnxTime
        if start_AnxTime == -999 or stop_AnxTime == -999:
            if force:
                # Loop over bursts
                if orbitDirection == "ascending":
                    for burstLatitudeIter, burstAzimuthAnxTimeIter in zip(burstLatitude, burstAzimuthAnxTime):
                        #print("Check 999 %s %s" %(burstLatitudeIter, burstAzimuthAnxTimeIter))
                        # Found a burst that is not far from start_lat
                        if (np.abs(burstLatitudeIter-start_lat)<dlatburst and start_AnxTime == -999):
                            start_AnxTime = float(burstAzimuthAnxTimeIter)
                        # Found a burst that is not far from stop_lat
                        if (np.abs(burstLatitudeIter-stop_lat)<dlatburst and stop_AnxTime == -999):
                            stop_AnxTime = float(burstAzimuthAnxTimeIter)
                    #print("Check 999 * %s %s" %(start_AnxTime, stop_AnxTime))
                else:
                    for burstLatitudeIter, burstAzimuthAnxTimeIter in zip(reversed(burstLatitude), reversed(burstAzimuthAnxTime)):
                        #print("Check 999 %s %s" %(burstLatitudeIter, burstAzimuthAnxTimeIter))
                        # Found a burst that is not far from start_lat
                        if (np.abs(burstLatitudeIter-start_lat)<dlatburst and stop_AnxTime == -999):
                            stop_AnxTime = float(burstAzimuthAnxTimeIter)
                        # Found a burst that is not far from stop_lat
                        if (np.abs(burstLatitudeIter-stop_lat)<dlatburst and start_AnxTime == -999):
                            start_AnxTime = float(burstAzimuthAnxTimeIter)
                    #print("Check 999 * %s %s" %(start_AnxTime, stop_AnxTime))
                logger.debug("%s: start / stop Anx times : %f / %f" %(sardir, start_AnxTime, stop_AnxTime))
                logger.debug("%s: found %s bursts " %(sardir, len(burstToKeep)))
                return (start_AnxTime, stop_AnxTime)
            else:
                start_AnxTime = float(burstAzimuthAnxTime[burstToKeep[0]])
                stop_AnxTime = float(burstAzimuthAnxTime[burstToKeep[-1]])

        #if start_AnxTime == -999 and stop_AnxTime == -999:
        #    if force:
        #        if orbitDirection == "ascending":
        #            if (burstLatitude[0] < start_lat and
        #                burstLatitude[-1] > stop_lat):
        #                # Check if burst just above or below to 
        #                # the required S or N cut is present
        #                print("Force check : %s" % ())
        #                if (burstLatitude[burstToKeep[0]] <
        #                    (start_lat+dlatburst) and
        #                    burstLatitude[burstToKeep[len(burstToKeep)-1]] > 
        #                    (stop_lat-dlatburst)):
        #                    start_AnxTime = float(burstAzimuthAnxTime[burstToKeep[0]])
        #                    stop_AnxTime = float(burstAzimuthAnxTime[burstToKeep[len(burstToKeep)-1]])
        #                else:
        #                    return (start_AnxTime, stop_AnxTime)
        #            else:
    #   #                 start_AnxTime = float(burstAzimuthAnxTime[burstToKeep[0]])
    #   #                 stop_AnxTime = float(burstAzimuthAnxTime[burstToKeep[len(burstToKeep)-1]])

        #                return(start_AnxTime, stop_AnxTime)
        #        else:
        #            if (burstLatitude[-1] < start_lat and 
        #                burstLatitude[0] > stop_lat):
        #                if (burstLatitude[burstToKeep[len(burstToKeep)-1]] < 
        #                    (start_lat+dlatburst) and 
        #                    burstLatitude[burstToKeep[0]] > (stop_lat-dlatburst)):
        #                    start_AnxTime = float(burstAzimuthAnxTime[burstToKeep[0]])
        #                    stop_AnxTime = float(burstAzimuthAnxTime[burstToKeep[len(burstToKeep)-1]])
        #                else:
    #   #                     start_AnxTime = float(burstAzimuthAnxTime[burstToKeep[0]])
    #   #                     stop_AnxTime = float(burstAzimuthAnxTime[burstToKeep[len(burstToKeep)-1]])
        #                    return (start_AnxTime, stop_AnxTime)
        #            else:
        #                return(start_AnxTime, stop_AnxTime)
        #    else:
        #        start_AnxTime = float(burstAzimuthAnxTime[burstToKeep[0]])
        #        stop_AnxTime = float(burstAzimuthAnxTime[burstToKeep[len(burstToKeep)-1]])
    #    if force :
    #       start_AnxTime = min(start_AnxTime,
    # float(burstAzimuthAnxTime[burstToKeep[0]]))
    #       stop_AnxTime  = max(float(burstAzimuthAnxTime
    # [burstToKeep[len(burstToKeep)-1]]),stop_AnxTime)


        # Refine selection based on Anx time
        logger.debug("%s: start / stop Anx times : %f / %f" %(sardir, start_AnxTime, stop_AnxTime))
        burstToKeep = []
        burstToKeep = [i for i in range(len(burstLatitude))
                       if (burstAzimuthAnxTime[i] > start_AnxTime - self.safetyMarginAnxTime) and
                          (burstAzimuthAnxTime[i] < stop_AnxTime + self.safetyMarginAnxTime)]
        logger.debug("%s: found %s bursts " %(sardir, len(burstToKeep)))

        if len(burstToKeep) == 0:
            safeDirList = list(set(burstSafedir))
            logger.info("Acquisition %s entirely outside of requested start / stop Anx time coverage!" % (date))
            for safeDir in safeDirList:
                logger.info(" Unused SAFE: {0:s}" % safeDir)
            return (start_AnxTime, stop_AnxTime)


        # List of all SAFE dirs
        safeDirList = list(set(burstSafedir))
        # Sort them in chronological order according to start time
        acquisitionStartTime = []
        for safeDir in safeDirList:
            [sensor, acquisitionMode, processingLevel, polarisationMode,
             acquisitionStartDate, acquisitionStopDate] = self.safenameParse(safeDir)
            acquisitionStartTime.append(datetime.datetime.strptime(acquisitionStartDate,
                                                          "%Y%m%dT%H%M%S"))
        timeSafeListSorted = sorted(zip(acquisitionStartTime, safeDirList))
        safeDirListSorted = [timeSafeListSorted[i][1]
                             for i in range(len(timeSafeListSorted))]
        # Loop over SAFE directories
        burstSkipBeg = []
        burstSkipEnd = []
        logger.debug("Looping over SAFE directories...")
        for safeDir in safeDirListSorted:
            logger.debug(" SAFE {0:s}".format(safeDir))
            # All global indexes of bursts in current SAFE
            indexInSafeDirAll = [i for i, j in enumerate(burstSafedir)
                                 if j == safeDir]
            # Global indexes of bursts in current SAFE that we will keep
            indexInSafeDirKeep = [val for val in burstToKeep
                                  if val in indexInSafeDirAll]
            # If at least one burst has to be kept
            if len(indexInSafeDirKeep) > 0:
                # Find how many bursts must be skipped at beginning and
                # end of current SAFE
                burstSkipBegCurrent = [i for i, j in enumerate(indexInSafeDirAll)
                                       if j == indexInSafeDirKeep[0]][0]
                burstSkipEndCurrent = [len(indexInSafeDirAll)-i-1
                                       for i, j in enumerate(indexInSafeDirAll)
                                       if j == indexInSafeDirKeep[-1]][0]
                logger.debug("  Skipping {0:d} bursts at beginning."
                          .format(burstSkipBegCurrent))
                logger.debug("  Skipping {0:d} bursts at end."
                          .format(burstSkipEndCurrent))
            # Otherwise, set to -99999 to report that file is out of latitude range
            else:
                burstSkipBegCurrent = -99999
                burstSkipEndCurrent = -99999
                logger.debug("  Unused SAFE (out of latitude range) : {0:s}".format(safeDir))
            burstSkipBeg.append(burstSkipBegCurrent)
            burstSkipEnd.append(burstSkipEndCurrent)
        # save to file, one line per SAFE directory
        #with open(os.path.join(sardir, date + "_burst_skip.txt"), 'w') as f:
        #    for FileNumber in range(len(safeDirListSorted)):
        #        f.write(str('{0:s} {1:9d} {2:9d}\n'.
        #                    format(safeDirListSorted[FileNumber],
        #                           burstSkipBeg[FileNumber],
        #                           burstSkipEnd[FileNumber])))

        # write selected bursts
        self.verif[sardir] = []
        #f = open(fileout, 'w')
        for BurstNumber in burstToKeep:
        #    f.write("%-80s %-15s %-15s %-15s %-15s\n"
        #            % (burstSafedir[BurstNumber],
        #               burstAzimuthTimeSeconds[BurstNumber],
        #               burstAzimuthAnxTime[BurstNumber],
        #               burstLatitude[BurstNumber],
        #               burstByteOffset[BurstNumber]))
            self.verif[sardir].append(burstAzimuthAnxTime[BurstNumber])
        #f.close()

        # write incomplete scenes + safe files belonging to incomplete scenes
        BurstNumber = burstToKeep[0]
        if burstAzimuthAnxTime[BurstNumber] > start_AnxTime + self.safetyMarginAnxTime:
            #self.fimout.write("%-15s %-15s %-15s %-15s %-15s\n"
            #             % (sardir, BurstNumber,
            #                burstAzimuthAnxTime[BurstNumber],
            #                start_AnxTime, "start"))
            for safeDir in safeDirList:
                #print("SAFE directory {0:s}".format(safeDir))
                #print("SAR directory {0:s}".format(sardir))
                logger.debug("  Unused SAFE (out of latitude range) : {0:s}".format(safeDir))
                #safeDirAbs = os.readlink(os.path.join(sardir, safeDir))
                #self.funused.write("{0:s}\n".format(safeDirAbs))
        BurstNumber = burstToKeep[-1]
        if burstAzimuthAnxTime[BurstNumber] < stop_AnxTime - self.safetyMarginAnxTime:
            #self.fimout.write("%-15s %-15s %-15s %-15s %-15s\n"
            #             % (sardir, BurstNumber,
            #                burstAzimuthAnxTime[BurstNumber],
            #                stop_AnxTime, "stop"))
            for safeDir in safeDirList:
                #print("\rUnused SAFE {0:s}\n".format(safeDir))
                #safeDirAbs = os.readlink(os.path.join(sardir, safeDir))
                logger.debug("  Unused SAFE (out of latitude range) : {0:s}".format(safeDir))
                #self.funused.write("{0:s}\n".format(safeDirAbs))

        return start_AnxTime, stop_AnxTime
        

    #############################################################################

    # Main function
    #if __name__ == "__main__":
    #
    #    parser = argparse.ArgumentParser(description="built ")
    #    parser.add_argument("procfile", type=str, help="proc file")
    #    parser.add_argument("-f", action="store_true", help="force")
    #    parser.add_argument("-a", action="store_true", help="auto")
    #    parser.add_argument("-d", action="store_true", help="deleteimages")
    #    parser.add_argument("-r", action="store_true", help="refine")
    #    parser.add_argument("-v", action="store_true", help="verbose")
    #    parser.add_argument("--factor_burst_time", type=str, help="factor_burst_time")
    #    parser.add_argument("--start_lat", type=str, help="start_latitudes")
    #    parser.add_argument("--stop_lat", type=str, help="stop_latitudes")

    def burst_selection_indep_first_pass(self, SarDirCatalogue, verbose=0):

        logger = nsb_utils.change_log_level(verbose ,[nsb_io, nsb_procparser, nsb_utils])
        
        # Run the processing for each sardir
        self.verif = {}
        self.verif_all = {}
        # First try to find the first date enclosing start_lat et stop_lat,
        # and define start_AnxTime, stop_AnxTime from it
        if (self.start_AnxTime == -999 or self.stop_AnxTime == -999) and self.force:
            #self.funused = open("UnusedSafe.tmp", 'w')
            #self.fimout = open("UnusedImage.tmp", 'w')
            start_AnxTime = self.start_AnxTime
            stop_AnxTime = self.stop_AnxTime
            for sardir in SarDirCatalogue:
                logger.debug("Extract metadata for %s" %sardir)
                myrsc, myburstpd, mygeolocpd, mygeoloccornerspd = self.safe2rsc(sardir, mode=self.swath, polarization=self.polarization, return_geoloc_corners=False, verbose=verbose, write_output=False)
                self.burstCatalogue[sardir] = myburstpd
                self.geoloc[sardir] = mygeolocpd
                [start_AnxTime, stop_AnxTime] = self.lat_cut_s1(self.burstCatalogue[sardir], sardir, self.swath, self.polarization,
                                                           self.start_lat, self.stop_lat,
                                                           start_AnxTime, 
                                                           stop_AnxTime,
                                                           self.force, verbose)
            #self.funused.close()
            #self.fimout.close()
            if (start_AnxTime == -999 or stop_AnxTime == -999) :
                logger.warning("No data covering entirely the given latitude range for date %s" % SarDirCatalogue )
                logger.warning("Please try with reduced latitude range")
                if( not self.auto_burst_selection):
                  sys.exit(1)
            else:
                [self.start_AnxTime, self.stop_AnxTime] = self.stat_burst(self.burstCatalogue, self.start_lat, self.stop_lat,
                                                       start_AnxTime,
                                                       stop_AnxTime,
                                                       self.refine, verbose)
                #if( not self.auto_burst_selection):
                #   self.proc.set("start_AnxTime", self.start_AnxTime)
                #   self.proc.set("stop_AnxTime", self.stop_AnxTime)
                #   with open(self.procfilename, "w") as f:
                #      self.proc.write(f)
        elif (self.auto_burst_selection):
            logger.warning("WARNING: option auto_burst_selection set to True, but start_AnxTime and stop_AnxTime are already set! Option auto_burst_selection will be ignored.")
            self.auto_burst_selection = False

        start_lat = self.start_lat
        stop_lat = self.stop_lat

        ##self.funused = open("UnusedSafe.txt", 'w')
        ##self.fimout = open("UnusedImage.txt", 'w')
        for sardir in SarDirCatalogue:
            logger.debug("Extract metadata for %s" %sardir)
            myrsc, myburstpd, mygeolocpd, mygeoloccornerspd = self.safe2rsc(sardir, mode=self.swath, polarization=self.polarization, verbose=verbose, write_output=False)
            self.burstCatalogue[sardir] = myburstpd
            self.geoloc[sardir] = mygeolocpd
            [self.start_AnxTime, self.stop_AnxTime] = self.lat_cut_s1(self.burstCatalogue[sardir], sardir, self.swath, self.polarization,
                                                       start_lat, stop_lat,
                                                       self.start_AnxTime, self.stop_AnxTime,
                                                       self.force, verbose)
                                                       
        if self.auto_burst_selection:
            return_geoloc_corners = True
            self.globalBursts, self.global_AnxTimes, self.array_of_burst, self.array_of_burst_select, self.auto_select_AnxTime, self.auto_select_dates, self.global_burstCorners = self.auto_select_burst(self.factor_burst_time, self.start_AnxTime, self.stop_AnxTime, return_geoloc_corners, verbose);

        #    if self.start_AnxTime == -999:
        #        self.start_AnxTime = start_AnxTime
        #    if self.stop_AnxTime == -999:
        #        self.stop_AnxTime = stop_AnxTime
        #    if not(self.force):
        #        if start_AnxTime < self.start_AnxTime:
        #            self.start_AnxTime = start_AnxTime
        #        if stop_AnxTime > self.stop_AnxTime:
        #            self.stop_AnxTime = stop_AnxTime

        #self.funused.close()
        #self.fimout.close()
        #print("Check verif (all) %s" % self.verif_all)
        #print("Check verif (1) %s" % self.verif)
        #print("Check verif * %s %s" %(self.start_AnxTime, self.stop_AnxTime))


        # Save to pickle
        #filename_pickle = 'pickle_verif'
        #with open(filename_pickle, "wb") as pickleFile:
        #    pickle.dump(self.verif_all, pickleFile)

    def burst_selection_auto_second_pass(self):#, factor_burst_time, return_geoloc_corners=False, verbose=False):
        
        # Run the algorithm
        return_geoloc_corners = True
        self.globalBursts, self.global_AnxTimes, self.array_of_burst, self.array_of_burst_select, self.auto_select_AnxTime, self.auto_select_dates, self.global_burstCorners = self.auto_select_burst(self.factor_burst_time, self.start_AnxTime, self.stop_AnxTime, return_geoloc_corners, self.verbose);
        
    def burst_selection_cleanup(self, verbose=False):
    
        if verbose:
            print("Check verif (2) %s" % self.verif)
        shutil.copy2(self.procfilename, self.procfilename + ".bak")
        with open("UnusedImage.txt") as f:
            sar2del = []
            for line in f:
                delSar = line.split()[0]
                sar2del.append(delSar)
            sar2del = list(set(sar2del))
            sar2del.sort()
            for delSar in sar2del:
                #utilproc.del_value_procfile(args.procfile, "SarDir", delSar)
                del_value_procfile(self.procfilename, "SarDir", delSar)
                if delSar in self.verif:
                    del self.verif[delSar]
                if verbose:
                    print("WARNING : image ", delSar, "deleted")

    def burst_check_end(self):
    
        # verify data in select burst file
        nbrburst = 0
        calmediane = []
        if verbose:
            print("Check verif (3) %s" % self.verif)
        for sardir in list(self.verif):
            if verbose:
                print("Check verif sardir %s " % sardir)
            nb = len(self.verif[sardir])
            calmediane.append(int(nb))
        nbrburst = np.median(calmediane)
        nbrburst = int(nbrburst)
        if verbose:
            print("Number of selected bursts :", nbrburst)
        #fnbb = open("NbBursts.txt", 'a')
        #fnbb.write("nbrburst=%i \n" % (nbrburst))
        #fnbb.close()

#        if deleteImage:
#            for sardir in list(self.verif):
#                nb = len(self.verif[sardir])
#                if nb != nbrburst:
#                    print("WARNING  Number of selected bursts are not equal !!!!")
#                    print("Relaunch with improved burst selection or with option -d -f")
#                    print("WARNING : image ", sardir)
#                    delSar = sardir
#                    utilproc.del_value_procfile(args.procfile, "SarDir", delSar)
#                    del self.verif[delSar]
#                    print("WARNING : image ", delSar, "deleted")
#                    fimout = open("UnusedImage.txt", 'a')
#                    fimout.write("%-15s \n" % (sardir))
#                    fimout.close()

        nbsafe = len(self.verif)
        for i in range(nbrburst):
            sumb = 0.0
            calmediane = []
            for val in self.verif.values():
               try:
                  b=val[i]
               except (IndexError, ValueError):
                  print ("WARNING  line:",i," incomplete")
               else:
                  sumb += float(val[i])
                  calmediane.append(float(val[i]))
            med = np.median(calmediane)
            moy = sumb/nbsafe
            print("line:", i, " moy:", moy, " med:", med)
            for sardir in list(self.verif):
               try:
                  b=self.verif[sardir][i]
               except (IndexError, ValueError):
                  print ("WARNING  line:",i," incomplete")
               else:
                  val = abs(med-float(self.verif[sardir][i]))
                  if val > 0.5:
                     print("WARNING  line:", i, " sardir:", sardir, " med:",
                          med, " value:", self.verif[sardir][i])
 
    def parse_args(self, SAFEsInSarDirs = None, swath = 'iw1', polarization = "vv", factor_burst_time = float(0.9), auto_burst_selection = True, start_lat = -89.9, stop_lat = 89.9, start_AnxTime = -999, stop_AnxTime = -999, safetyMarginLatitude = float(0.07), safetyMarginAnxTime = float(0.5), force=False, deleteImage=False, refine=False, verbose = 0):

        # Verbose
        if int(verbose) in [0, 1, 2, 3, 4]:
            self.verbose = int(verbose)
        else:
            logger.warning("Argument verbose not understood. Should be True or False or 0, 1, 2, 3, or 4. Set to 0 (=False).")
            self.verbose = int(0)
        logger = nsb_utils.change_log_level(self.verbose ,[nsb_io, nsb_procparser, nsb_utils])
            
        # List of SAFEs for each SAR dir
        self.SAFEsInSarDirs = SAFEsInSarDirs

        # List of SAR dirs (one per date)
        self.SarDirCatalogue = sorted(SAFEsInSarDirs.keys())
        
        # Safety margin to ensure that prescribed latitudes are not
        # too close to burst latitude
        self.safetyMarginLatitude = safetyMarginLatitude

        # Safety margin to ensure that prescribed AnX time is not too restrictive
        self.safetyMarginAnxTime = safetyMarginAnxTime

        self.swath = swath
        self.polarization = polarization

        self.force = force
        self.deleteImage = deleteImage
        self.refine = refine

        # Read start / stop latitudes
        self.start_lat = start_lat
        self.stop_lat = stop_lat

        # # Start / stop times, if present in procfile
        self.start_AnxTime = start_AnxTime
        self.stop_AnxTime = stop_AnxTime

        
        # Automatic burst selection
        self.factor_burst_time = factor_burst_time
        self.auto_burst_selection = auto_burst_selection            

    def run_nsb_prep_slc_latcut_s1_new(self):
        
        # Run first pass
        self.burst_selection_indep_first_pass(self.SarDirCatalogue)

        # Automatic burst selection
        if self.auto_burst_selection:
            self.burst_selection_auto_second_pass()

        # Post-processing
        #if deleteImage:
        #    self.burst_selection_cleanup()
        self.burst_check_end()
        
    def __init__(self):
        
        self.burstCatalogue = {}
        self.geoloc = {}
        self.SarDirCatalogue = []
        self.SAFEsInSarDirs = {}
        #self.globalBursts = None
        #self.global_AnxTimes = None
        #self.array_of_burst = None
        #self.array_of_burst_select = None
        #self.global_burstCorners = None



# * Copyright (C) 2016-2022 R.GRANDIN
#
# * grandin@ipgp.fr
#
# * This file is part of "Sentinel-1 pre-processor for ROI_PAC".
#
# *    "Sentinel-1 pre-processor for ROI_PAC" is free software: you can redistribute
#      it and/or modify it under the terms of the GNU General Public License
#        as published by the Free Software Foundation, either version 3 of
#        the License, or (at your option) any later version.
#
# *    "Sentinel-1 pre-processor for ROI_PAC" is distributed in the hope that it
#      will be useful, but WITHOUT ANY WARRANTY; without even the implied
#        warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#        See the GNU General Public License for more details.
#
# *     You should have received a copy of the GNU General Public License
#      along with "Sentinel-1 pre-processor for ROI_PAC".
#        If not, see <http://www.gnu.org/licenses/>.
