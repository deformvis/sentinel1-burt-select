#!/usr/bin/env python3

import os, sys, re
import glob
import xml.etree.ElementTree
from io import StringIO, BytesIO
import zipfile

import nsbasBursts
import nsbasBursts.nsbio as nsb_io
import nsbasBursts.procparser as nsb_procparser
import nsbasBursts.utils as nsb_utils
import nsbasBursts.remotezip as nsb_remotezip

# Check if we need to work:
#  1. on a zipped SAFE, or a plain SAFE
#  2. if the zip is local or remote
def check_SAFE_type_zip_local(safepath, logger):
    if os.path.splitext(safepath)[-1] == '.zip':
        zippedSafe = True # Read from within a zipped SAFE
        logger.info("Working on a Zipped file in %s" % safepath)
        zipIsLocal = guess_Zip_local_or_remote(safepath, logger)
        # Adjust VSI prefix accordingly
        if zipIsLocal:
            zipPrefixVSI='/vsizip/'
        else:
            zipPrefixVSI='/vsizip/vsicurl/'
    else:
        zippedSafe = False # Normal behavior
        zipIsLocal = None
        zipPrefixVSI = ''
    return(zippedSafe, zipIsLocal, zipPrefixVSI)


# Find if zip file is local or remote
# Returns True if local, False if remote
def guess_Zip_local_or_remote(test_filein_zip, logger):

    # Store zip file content
    try:
        # Try to read the zip naively
        zipsafe = zipfile.ZipFile(test_filein_zip)
    except OSError as e:
        # Failure: this must be a remote zip
        logger.info("Zip appears to be remote")
        zipIsLocal = False
    else:
        # Okay, the zip is stored locally
        logger.info("Zip appears to be local.")
        zipIsLocal = True
    return(zipIsLocal)

# Extract file list in Zip
# Zip can be local or distant
def get_zip_file_listing(filename_zip, islocal, logger):

    # Is it a local or a remote Zip?
    if islocal is None:
        islocal = guess_Zip_local_or_remote(filename_zip, logger)
    # Local Zip : use plain zipfile library
    if islocal:
        logger.info("Extracting file listing from local zip.")
        zipsafe = zipfile.ZipFile(filename_zip)
    else:
    # Remote Zip : use Remotezip library
    # From : https://pypi.org/project/remotezip/#history
        logger.info("Extracting file listing from remote zip.")
        zipsafe = nsb_remotezip.RemoteZip(filename_zip)
    zipsafe_listing = zipsafe.namelist()
    return zipsafe_listing

# Find SAFE name from list of files found in the zip
def get_SAFE_name_from_Zip(file_listing_in_zip, logger):
    safename = os.path.basename(os.path.dirname(file_listing_in_zip[0]))
    logger.info("SAFE name found in zip : %s " % safename)
    return safename

# Find TIFF name from list of files found in the zip, for a given swath and polarization
def get_TIFF_name_from_Zip(file_listing_in_zip, swath, polarization, logger):

    reg = re.compile("s1.-"+swath+"-slc-"+polarization)
    tiffname = None
    for filename in file_listing_in_zip:
        if bool(re.match(reg, os.path.basename(filename)[0:14])) \
                and os.path.basename(filename).endswith('tiff'):
            tiffname = os.path.basename(filename)
            break
    logger.info("TIFF name found in zip : %s " % tiffname)
    return tiffname

# Read annotation XML from a zipped SAFE
# Zip can be local or distant
def read_XML_from_Zip(filename_zip, safename, filein_zip_xml, islocal, logger):

    # Is it a local or a remote Zip?
    if islocal is None:
        islocal = guess_Zip_local_or_remote(filename_zip, logger)
    # Parse the xml
    if islocal:
        zipsafe = zipfile.ZipFile(filename_zip)
    else:
        zipsafe = nsb_remotezip.RemoteZip(filename_zip)
    logger.info("Reading XML %s from %s as %s" % (filein_zip_xml, filename_zip, zipsafe))
    t_str = str('')
    with zipsafe.open(os.path.join(safename, 'annotation', filein_zip_xml)) as f:
        for line in f:
            t_str = t_str + line.decode("utf-8")
    t = xml.etree.ElementTree.fromstring(t_str)
    return t

# Read IPF version from a zipped SAFE
# Zip can be local or distant
def get_ipfversion_from_zipped_manifest(filename_zip, safename, islocal, logger):

    # Is it a local or a remote Zip?
    if islocal is None:
        islocal = guess_Zip_local_or_remote(filename_zip, logger)
    # Parse the xml
    if islocal:
        zipsafe = zipfile.ZipFile(filename_zip)
    else:
        zipsafe = nsb_remotezip.RemoteZip(filename_zip)
    filename_manifest = os.path.join(safename, 'manifest.safe')
    logger.info("Reading IPF version from manifest %s in %s as %s" % (filename_manifest, safename, zipsafe))
    t_str = str('')
    with zipsafe.open(filename_manifest) as f:
        for line in f:
            if "software name=\"Sentinel-1 IPF\" version=" in line.decode("utf-8"):
                ipfversion = float(line.decode("utf-8").split("\"")[-2])
                break
    return ipfversion

# Extract IPF version from a SAFE directory 
# The SAFE can be unzipped, or zipped inside a local or distant zip
def get_ipfversion_from_manifest(safepath, zippedSafe, islocal, logger):

    if zippedSafe:
        # Is it a local or a remote Zip?
        if islocal is None:
            islocal = guess_Zip_local_or_remote(safepath, logger)
        # Read the Zip file listing
        zipFileListing = get_zip_file_listing(safepath, islocal, logger)
        # Determine file names
        safename_inzip = get_SAFE_name_from_Zip(zipFileListing, logger)
        ipfversion = get_ipfversion_from_zipped_manifest(safepath, safename_inzip, islocal, logger)
    else:
        globpat = os.path.join(safepath, "manifest.safe")
        manifestfile = glob.glob(os.path.join(globpat))
        if len(manifestfile) == 0:
            return None
        # Read IPF version in manifest (simple grep)
        logger.info("Reading IPF version from manifest %s in %s" % (manifestfile[0], safepath))
        with open(manifestfile[0]) as f:
            for line in f:
                if "software name=\"Sentinel-1 IPF\" version=" in line:
                    ipfversion = float(line.split("\"")[-2])
                    break
    return ipfversion

# Read IPF version from Tiff metadata
def get_ipfversion_from_tiff_metadata(src_ds):
    src_ds_metadata = src_ds.GetMetadata()
    ipfversion = float(src_ds_metadata['TIFFTAG_SOFTWARE'].split()[-1])
    return ipfversion

# Read metadata from annotation XML in a SAFE, for a given swath and polarization
# The SAFE can be unzipped, or zipped inside a local or distant zip
def open_annotationxml(safepath, swath, polarization, zippedSafe, islocal, logger):

    if zippedSafe:
        # Is it a local or a remote Zip?
        if islocal is None:
            islocal = guess_Zip_local_or_remote(safepath, logger)
        # Read the Zip file listing
        zipFileListing = get_zip_file_listing(safepath, islocal, logger)
        # Determine file names
        safename_inzip = get_SAFE_name_from_Zip(zipFileListing, logger)
        filename_tif_inzip = get_TIFF_name_from_Zip(zipFileListing, swath, polarization, logger)
        filename_xml_inzip = os.path.splitext(filename_tif_inzip)[0] + '.xml'
        doc = read_XML_from_Zip(safepath, safename_inzip, filename_xml_inzip, islocal, logger) 
    else:
        globpat = os.path.join(safepath,
                               "annotation",
                               "*-%s-slc-%s-*.xml" % (swath, polarization))
        g = glob.glob(os.path.join(globpat))
        if g is None:
            return None
        if len(g) != 1:
            return None
        doc = xml.etree.ElementTree.ElementTree()
        doc.parse(g[0])
    return doc

# Get the root path to enable GDAL to read TIF files inside a Zip
# Zip file can be local or remote
def get_VSI_root_path(safepath, zippedSafe, islocal, logger):

    if zippedSafe:
        # Is it a local or a remote Zip?
        if islocal is None:
            islocal = guess_Zip_local_or_remote(safepath, logger)
        # Adjust VSI prefix accordingly
        if islocal:
            zipPrefixVSI='/vsizip/'
        else:
            zipPrefixVSI='/vsizip/vsicurl/'
        vsipath = zipPrefixVSI + safepath
        logger.info("Using virtual path prefix for GDAL : %s" % zipPrefixVSI)
    else:
        vsipath = '.'
    return vsipath

# Get full path of file to enabel GDAL to read TIF
# There are three cases (can be combined):
#  1. Plain tiff file
#  2. Tiff file inside a zipped SAFE
#  3. Tiff file inside a SAFE stored remotely (needs range request capability)
def get_VSI_tiff_path(safepath, swath, polarization, zippedSafe, islocal, logger):

    # Determine root path for GDAL
    safeRootPathForGdal = get_VSI_root_path(safepath, zippedSafe, islocal, logger) 
    logger.info("    safeRootPathForGdal %s" % safeRootPathForGdal)

    if zippedSafe:
        # Is it a local or a remote Zip?
        if islocal is None:
            islocal = guess_Zip_local_or_remote(safepath, logger)
        # Read the Zip file listing
        zipFileListing = get_zip_file_listing(safepath, islocal, logger)
        # Determine file names
        safename_inzip = get_SAFE_name_from_Zip(zipFileListing, logger)
        filename_tif = get_TIFF_name_from_Zip(zipFileListing, swath, polarization, logger)
        filepath_tif = os.path.join(safename_inzip, 'measurement', filename_tif)
    else:
        safename_inzip = ''
        globpat = os.path.join(safepath,
                               "measurement",
                               "*-%s-slc-%s-*.tiff" % (swath, polarization))
        filename_tif_list = glob.glob(os.path.join(globpat))
        if len(filename_tif_list) == 0:
            filepath_tif = None
        else:
            filepath_tif = filename_tif_list[0]
    tiff_full_path_for_GDAL = os.path.join(safeRootPathForGdal, filepath_tif)
    logger.info("Full path to tiff for GDAL : %s" % tiff_full_path_for_GDAL)
    return tiff_full_path_for_GDAL

# Read orbit info in a remote zip
def read_orbit_in_remote_zip(filename_zip, filename_orbit, islocal, logger):
    # Is it a local or a remote Zip?
    if islocal is None:
        islocal = guess_Zip_local_or_remote(filename_zip, logger)
    # Parse the xml
    if islocal:
        zipsafe = zipfile.ZipFile(filename_zip)
    else:
        zipsafe = nsb_remotezip.RemoteZip(filename_zip)
    filename_orbit_full = os.path.join(filename_zip, filename_orbit)
    logger.info("Reading zipped orbit file %s in %s" % (filename_orbit, filename_zip))
    t_str = str('')
    with zipsafe.open(filename_orbit) as f:
        for line in f:
            t_str += line.decode("utf-8")
    return t_str





