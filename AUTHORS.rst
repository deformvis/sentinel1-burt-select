Contributors
============

Here are a list of people that work, or have worked, on insarviz.

* UGA stands for University Grenoble Alpes (https://www.univ-grenoble-alpes.fr/).
* ISTerre stands for Institut des Sciences de la Terre (https://www.isterre.fr).
* IPGP stands for Institut Physique du Globle

Current contributors
--------------------

Alphabetic order of last name : 

* Raphaël Grandin (IPGP) grandin@ipgp.fr
* Franck Thollard (ISTerre/CNRS) franck.thollard@univ-grenoble-alpes.fr


Contact
-------

Franck Thollard <franck.thollard@univ-grenoble-alpes.fr>

